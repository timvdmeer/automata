package regex;

import java.util.List;

import automata.NFA;
import exceptions.RegexException;

public class Star extends Operator
{
	private RegexElement operand;
	
	public Star(RegexElement operand)
	{
		this.operand = operand;
	}
	
	public RegexElement getOperand()
	{
		return operand;
	}
	
	public String toString()
	{
		if (operand instanceof Operator)
		{
			return "(" + operand.toString() + ")*";
		}
		return operand.toString() + "*";
	}
	
	public NFA convertToNFA() throws RegexException
	{
		return NFA.NFAStar(operand.convertToNFA());
	}
	
	public List<Symbol> getSymbols()
	{
		return operand.getSymbols();
	}
}
