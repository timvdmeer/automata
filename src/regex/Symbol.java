package regex;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import automata.NFA;
import automata.State;
import automata.Transition;

public class Symbol extends RegexElement
{
	public static final String EPSILON = "-";
	public static final String EMPTY_LANGUAGE = "empty_language";
	
	private String symbol;
	
	private int position = -1; // used in Glushkov automaton construction, unused otherwise (indicated by -1)
	private Set<Symbol> follow = new HashSet<>(); // used in Glushkov automaton construction, unused otherwise
	
	public Symbol(String symbol)
	{
		this.symbol = symbol;
	}
	
	public Symbol(String symbol, int position)
	{
		this.symbol = symbol;
		this.position = position;
	}
	
	public Symbol(char symbol)
	{
		this.symbol = String.valueOf(symbol);
	}
	
	public Symbol(int symbol)
	{
		this.symbol = String.valueOf(symbol);
	}
	
	public String getSymbol()
	{
		return symbol;
	}
	
	public int getPosition()
	{
		return position;
	}
	
	public void setPosition(int position)
	{
		this.position = position;
	}
	
	public void setFollow(Set<Symbol> follow)
	{
		this.follow = follow;
	}
	
	public Set<Symbol> getFollow()
	{
		return follow;
	}
	
	public String toString()
	{
		// append the position if relevant
		if (position  != -1)
		{
			return symbol + "{" + position + "}";
		}
		return symbol;
	}
	
	public boolean isEpsilon()
	{
		return symbol.equals(Symbol.EPSILON);
	}
	
	public boolean isEmptyLanguage()
	{
		return symbol.equals(Symbol.EMPTY_LANGUAGE);
	}
	
	public NFA convertToNFA()
	{
		Set<State> states = new HashSet<>();
		Set<String> alphabet = new HashSet<>();
		Set<Transition> transitionFunction = new HashSet<>();
		Set<State> acceptStates = new HashSet<>();
		
		State startState = new State();
		State acceptState = new State();
		states.add(startState);
		states.add(acceptState);
		alphabet.add(symbol);
		transitionFunction.add(new Transition(startState, symbol, acceptState));
		acceptStates.add(acceptState);
		
		return new NFA(states, alphabet, transitionFunction, startState, acceptStates);
	}
	
	public List<Symbol> getSymbols()
	{
		List<Symbol> list = new ArrayList<>();
		list.add(new Symbol(symbol, position));
		return list;
	}
}
