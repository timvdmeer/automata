package regex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import automata.NFA;
import exceptions.RegexException;

public class Concatenation extends Operator
{
	private List<RegexElement> elements;
	
	public Concatenation(RegexElement left, RegexElement right)
	{
		elements = new ArrayList<>();
		elements.add(left);
		elements.add(right);
	}
	
	public Concatenation(List<RegexElement> elements) throws RegexException
	{
		if (elements.size() < 2)
		{
			throw new RegexException("A concatenation cannot have less than two elements!");
		}
		this.elements = elements;
	}
	
	public List<RegexElement> getElements()
	{
		return elements;
	}
	
	public String toString()
	{
		String result = "";
		
		for (RegexElement element : elements)
		{
			if (element instanceof Union)
			{
				result += "(" + element.toString() + ")";
			}
			else
			{
				result += element.toString();
			}
			result += "+";
		}
		result = result.substring(0, result.length() - 1);
		
		return result;
	}
	
	public NFA convertToNFA() throws RegexException
	{
		Iterator<RegexElement> it = elements.iterator();
		if (it.hasNext())
		{
			NFA root = it.next().convertToNFA();
			while (it.hasNext())
			{
				root = NFA.NFAConcatenation(root, it.next().convertToNFA());
			}
			return root;
		}
		// this code should never be reached
		throw new RegexException("This concatenation does not have any elements in its list!");
	}
	
	public List<Symbol> getSymbols()
	{
		List<Symbol> list = new ArrayList<>();
		for (RegexElement element : elements)
		{
			list.addAll(element.getSymbols());
		}
		return list;
	}
	
	public BinaryConcatenation convertToBinaryConcatenationTree()
	{
		return null;
	}
}
