package regex;

import java.util.ArrayList;
import java.util.List;

import automata.NFA;
import exceptions.RegexException;

public class BinaryConcatenation extends Operator
{
	private RegexElement left;
	private RegexElement right;
	
	public BinaryConcatenation(RegexElement left, RegexElement right)
	{
		this.left = left;
		this.right = right;
	}
	
	public BinaryConcatenation(List<RegexElement> elements) throws RegexException
	{
		if (elements.size() != 2)
		{
			throw new RegexException("A binary concatenation must have exactly two elements!");
		}
		left = elements.get(0);
		right = elements.get(1);
	}
	
	public RegexElement getLeft()
	{
		return left;
	}
	
	public RegexElement getRight()
	{
		return right;
	}
	
	public String toString()
	{
		String result = "";
		
		if (left instanceof Operator)
		{
			result += "(" + left.toString() + ")";
		}
		else
		{
			result += left.toString();
		}
		result += "+";
		if (right instanceof Operator)
		{
			result += "(" + right.toString() + ")";
		}
		else
		{
			result += right.toString();
		}
		
		return result;
	}
	
	public NFA convertToNFA() throws RegexException
	{
		return NFA.NFAConcatenation(left.convertToNFA(), right.convertToNFA());
	}
	
	public List<Symbol> getSymbols()
	{
		List<Symbol> list = new ArrayList<>();
		list.addAll(left.getSymbols());
		list.addAll(right.getSymbols());
		return list;
	}
}
