package regex;

import java.util.List;

import automata.NFA;
import exceptions.RegexException;

public class Union extends Operator
{
	private RegexElement left;
	private RegexElement right;
	
	public Union(RegexElement left, RegexElement right)
	{
		this.left = left;
		this.right = right;
	}
	
	public RegexElement getLeft()
	{
		return left;
	}
	
	public RegexElement getRight()
	{
		return right;
	}
	
	public String toString()
	{
		return left.toString() + "|" + right.toString();
	}
	
	public NFA convertToNFA() throws RegexException
	{
		return NFA.NFAUnion(left.convertToNFA(), right.convertToNFA());
	}
	
	public List<Symbol> getSymbols()
	{
		List<Symbol> list = left.getSymbols();
		list.addAll(right.getSymbols());
		return list;
	}
}
