package regex;

import java.util.List;
import java.util.Set;

import automata.NFA;
import exceptions.RegexException;

public abstract class RegexElement
{
	private boolean nullable;
	private Set<Symbol> first;
	private Set<Symbol> last;
	
	public void setNullable(boolean nullable)
	{
		this.nullable = nullable;
	}
	
	public void setFirst(Set<Symbol> first)
	{
		this.first = first;
	}
	
	public void setLast(Set<Symbol> last)
	{
		this.last = last;
	}
	
	public boolean getNullable()
	{
		return nullable;
	}
	
	public Set<Symbol> getFirst()
	{
		return first;
	}
	
	public Set<Symbol> getLast()
	{
		return last;
	}
	
	/**
	 * Prints the contents of this element.
	 * @return a string containing the contents of this element
	 */
	public abstract String toString();
	
	/**
	 * Converts this node and its children to a NFA.
	 * @return a NFA representation of this regex node
	 */
	public abstract NFA convertToNFA() throws RegexException;
	
	/**
	 * Lists every symbol in this node and its children.
	 * @return a list containing every symbol that is a descendant of this node (including itself in case of this node being a symbol)
	 */
	public abstract List<Symbol> getSymbols();
}
