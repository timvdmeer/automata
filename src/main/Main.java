package main;

import java.io.IOException;
import java.util.List;

import automata.*;
import exceptions.*;
import regex.*;

public class Main
{
	private static enum Construct {REGEX, NFA, DFA, GNFA};
	private static enum MinimizeOptions {MINIMIZE, MINIMIZE_AND_REPLACE, DO_NOT_MINIMIZE};
	
	public static void main(String[] args)
	{
		try
		{
			Log.setLogLevel(Log.LogLevel.INFO);
			
			List<RegexElement> regexList = Utility.parseRegexFromJSONFile("resources/mathml.json");
			//RegexElement regex = regexList.get(0); //regexList.size() - 22); //22);
			
			for (RegexElement regex : regexList)
			{
				
				Log.info("Parsed regex:");
				Log.info(regex.toString());
				NFA thompsonNFA = regex.convertToNFA();
				int transitionCountNFA = thompsonNFA.transitionCount();
				int stateCountNFA = thompsonNFA.getStates().size();
				NFA glushkovNFA = thompsonNFA.convertToGlushkovAutomaton();
				DFA dfa = glushkovNFA.convertToDFA();
				DFA minimizedDFA = dfa.minimize(true);
				int transitionCountDFA = minimizedDFA.transitionCount();
				int stateCountDFA = minimizedDFA.getStates().size();
				Log.info("States:\n\t[Original NFA]: " + stateCountNFA + "\n\t[Minimized DFA]: " + stateCountDFA);
				if (stateCountDFA < stateCountNFA)
				{
					double percentage = 100 - ((double)stateCountDFA / (double)stateCountNFA * 100);
					Log.info("\tThe optimisation resulted in a " + percentage + "% reduction in states.");
				}
				else if (stateCountDFA > stateCountNFA)
				{
					double percentage = ((double)stateCountDFA / (double)stateCountNFA * 100) - 100;
					Log.info("\tThe optimisation resulted in a " + percentage + "% increase in states.");
				}
				else
				{
					Log.info("\tThe optimisation did not result in a change in the amount of states.");
				}
				Log.info("Transitions:\n\t[Original NFA]: " + transitionCountNFA + "\n\t[Minimized DFA]: " + transitionCountDFA);
				if (transitionCountDFA < transitionCountNFA)
				{
					double percentage = 100 - ((double)transitionCountDFA / (double)transitionCountNFA * 100);
					Log.info("\tThe optimisation resulted in a " + percentage + "% reduction in transitions.");
				}
				else if (transitionCountDFA > transitionCountNFA)
				{
					double percentage = ((double)transitionCountDFA / (double)transitionCountNFA * 100) - 100;
					Log.info("\tThe optimisation resulted in a " + percentage + "% increase in transitions.");
				}
				else
				{
					Log.info("\tThe optimisation did not result in a change in the amount of transitions.");
				}
				Log.info("Transition report of original NFA:");
				thompsonNFA.printTransitionReport();
				Log.info("Transition report of optimized DFA:");
				minimizedDFA.printTransitionReport();
				Log.info("----------------------------------------------------------------------------------");
			}
			
			
			Main.exit();
			
			
			
			
			
			
//			RegexElement regex = Utility.parseRegexFromFile("resources/regex13.txt", true);
			RegexElement regex = null;
			Log.info("Parsed regex:");
			Log.info(regex.toString());
			Log.info("Converting regex to Thompson NFA...");		
			NFA thompsonNFA = regex.convertToNFA();
			//Log.info(thompsonNFA.toString());
			thompsonNFA.printTransitionReport();
			Log.info("Converting Thompson NFA to Glushkov NFA...");
			
			NFA glushkovNFA = thompsonNFA.convertToGlushkovAutomaton();
//			NFA glushkovNFA = NFA.createNFA("resources/nfa5.txt");
			
			Log.info("Converting Glushkov NFA to DFA...");
			DFA dfa = glushkovNFA.convertToDFA();
			Log.info("Minimizing DFA...");
			DFA minimizedDFA = dfa.minimize(true);
			Log.info("[Minimized DFA]");
			Log.info(minimizedDFA.toString());
			
			Main.exit();
			
			test(Construct.REGEX, "resources/regex1.txt", Construct.DFA, MinimizeOptions.MINIMIZE_AND_REPLACE);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void exit()
	{
		Log.info("END OF LOG");
		Log.info("-------------------------------------------------------------------------------------------------");
		Log.writeLogBufferToFile();
		System.exit(0);
	}
	
	private static void test(Construct from, String path, Construct to, MinimizeOptions minimizeOptions) throws IOException, ParseException, RegexException, StateException, DuplicateTransitionException, IllFormedAutomatonException
	{
		RegexElement regex1 = null;
		NFA nfa = null;
		DFA dfa = null;
		GNFA gnfa = null;
		
		switch (from)
		{
			case REGEX:
				regex1 = parseRegex(path);
				nfa = convertToNFA(regex1); if (to == Construct.NFA){return;}
				dfa = convertToDFA(nfa); dfa = minimizeDFA(dfa, minimizeOptions); if (to == Construct.DFA){return;}
				gnfa = convertToGNFA(dfa); if (to == Construct.GNFA){return;}
				convertToRegex(gnfa);
				break;
				
			case NFA:
				nfa = parseNFA(path); if (to == Construct.NFA){return;}
				dfa = convertToDFA(nfa); dfa = minimizeDFA(dfa, minimizeOptions); if (to == Construct.DFA){return;}
				gnfa = convertToGNFA(dfa); if (to == Construct.GNFA){return;}
				convertToRegex(gnfa);
				break;
				
			case DFA:
				dfa = parseDFA(path); dfa = minimizeDFA(dfa, minimizeOptions); if (to == Construct.DFA){return;}
				gnfa = convertToGNFA(dfa); if (to == Construct.GNFA){return;}
				convertToRegex(gnfa);
				break;
				
			case GNFA:
				Log.fatal("Cannot start from a GNFA!");
				System.exit(0);
				break;
		}
	}
	
	private static NFA parseNFA(String path) throws IOException
	{
		NFA nfa = NFA.createNFA(path);
		Log.info(nfa.toString());
		Log.info("---------------------------------");
		
		return nfa;
	}
	
	private static DFA parseDFA(String path) throws IOException, DuplicateTransitionException
	{
		DFA dfa = DFA.createDFA(path);
		Log.info(dfa.toString());
		Log.info("---------------------------------");
		
		return dfa;
	}
	
	private static RegexElement parseRegex(String path) throws IOException, ParseException
	{
		Log.info("[REGEX]");
		RegexElement regex = Utility.parseRegexFromFile(path, false);
		Log.info("Printing parsed AST:");
		Log.info(regex.toString());
		Log.info("---------------------------------");
		
		return regex;
	}
	
	private static NFA convertToNFA(RegexElement regex) throws RegexException
	{
		NFA nfa = regex.convertToNFA();
		Log.info(nfa.toString());
		Log.info("---------------------------------");
		
		return nfa;
	}
	
	private static DFA convertToDFA(NFA nfa) throws StateException, DuplicateTransitionException
	{
		DFA dfa = nfa.convertToDFA();
		Log.info(dfa.toString());
		Log.info("---------------------------------");
		
		return dfa;
	}
	
	private static DFA minimizeDFA(DFA dfa, MinimizeOptions minimizeOptions) throws IllFormedAutomatonException, DuplicateTransitionException, StateException
	{
		DFA minimizedDFA = null;
		switch (minimizeOptions)
		{
			case DO_NOT_MINIMIZE:
				return dfa;
				
			case MINIMIZE:
				minimizedDFA = dfa.minimize(false);
				break;
				
			case MINIMIZE_AND_REPLACE:
				minimizedDFA = dfa.minimize(true);
				break;
		}
		Log.info("[Minimized DFA]");
		Log.info(minimizedDFA.toString());
		Log.info("---------------------------------");
		
		return minimizedDFA;
	}
	
	private static GNFA convertToGNFA(DFA dfa)
	{
		GNFA gnfa = dfa.convertToGNFA();
		Log.info(gnfa.toString());
		Log.info("---------------------------------");
		
		return gnfa;
	}
	
	private static RegexElement convertToRegex(GNFA gnfa) throws RegexException, IllFormedAutomatonException
	{
		RegexElement root = gnfa.convertToRegex();
		Log.info("[REGEX]");
		Log.info(root.toString());
		
		return root;
	}
}
