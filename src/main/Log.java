package main;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JTextArea;

/**
 * Provides support for logging capabilities.
 */
public class Log
{
	/**
	 * Enumerates the possible log levels.
	 */
	public static enum LogLevel
	{
		DEBUG("DEBUG: "),
		INFO("INFO : "),
		WARN("WARN : "),
		ERROR("ERROR: "),
		FATAL("FATAL: ");
		
		private final String logLevel;
		
		private LogLevel(String logLevel)
		{
			this.logLevel = logLevel;
		}
		
		private String logLevel()
		{
			return logLevel;
		}
	}
	
	private static LogLevel logLevel = LogLevel.DEBUG;
	private static PrintStream logStream = System.out;
	private static boolean logToFile = true;
	private static int logBufferSize = 500;
	private static int count = 0;
	private static String[] logBuffer = new String[logBufferSize];
	private static boolean printTimestamp = false;
	private static boolean printLogLevel = false;
	
	private static JTextArea textArea;
	private static boolean outputToArea = false;
	
	public static void setOutputToFrame(JTextArea text)
	{
		textArea = text;
		outputToArea = true;
	}
	
	public static void setLogToFile(boolean logFile)
	{
		logToFile = logFile;
	}
	
	public static void setLogStream(PrintStream stream)
	{
		logStream = stream;
	}
	
	public static PrintStream getLogStream()
	{
		return logStream;
	}
	
	public static void setLogLevel(LogLevel logLevel)
	{
		Log.logLevel = logLevel;
	}
	
	public static LogLevel getLogLevel()
	{
		return logLevel;
	}
	
	public static void setLogBufferSize(int logBufferSize)
	{
		// if the new log buffer size is smaller than the old, write the buffer first before 
		// putting a new, smaller array in its place
		if (Log.logBufferSize > logBufferSize)
		{
			writeLogBufferToFile();
		}
		logBuffer = Arrays.copyOf(logBuffer, logBufferSize);
	}
	
	public static int getLogBufferSize()
	{
		return logBufferSize;
	}
	
	/**
	 * Prints the message to the current output stream if the log level of the message is lower or equal to the
	 * global log level.
	 * @param message - the message to be printed
	 * @param logLevel - the log level of the message
	 */
	private static void printIfCorrectLogLevel(String message, LogLevel logLevel)
	{
		if (checkLogLevel(getLogLevel(), logLevel))
		{
			if (printLogLevel)
			{
				print(logLevel.logLevel() + message);
			}
			else
			{
				print(message);
			}
		}
	}
	
	/**
	 * Checks whether the given log level is lower or equal to the master log level.
	 * @param masterLogLevel - the log level 
	 * @param logLevel - the log level 
	 * @return
	 */
	private static boolean checkLogLevel(LogLevel masterLogLevel, LogLevel logLevel)
	{
		switch (masterLogLevel)
		{
			case DEBUG:
				return true;
			case INFO:
				return logLevel == LogLevel.INFO || logLevel == LogLevel.WARN || logLevel == LogLevel.ERROR || logLevel == LogLevel.FATAL;
			case WARN:
				return logLevel == LogLevel.WARN || logLevel == LogLevel.ERROR || logLevel == LogLevel.FATAL;
			case ERROR:
				return logLevel == LogLevel.ERROR || logLevel == LogLevel.FATAL;
			case FATAL:
				return logLevel == LogLevel.FATAL;
			default:
				return false;
		}
	}
	
	public static synchronized void debug(String message)
	{
		printIfCorrectLogLevel(message, LogLevel.DEBUG);
	}
	
	public static synchronized void info(String message)
	{
		printIfCorrectLogLevel(message, LogLevel.INFO);
	}
	
	public static synchronized void warn(String message)
	{
		printIfCorrectLogLevel(message, LogLevel.WARN);
	}
	
	public static synchronized void error(String message)
	{
		printIfCorrectLogLevel(message, LogLevel.ERROR);
	}
	
	public static synchronized void fatal(String message)
	{
		printIfCorrectLogLevel(message, LogLevel.FATAL);
	}
	
	public static synchronized void error(Exception e)
	{
		error(e.toString());
		for (StackTraceElement element : e.getStackTrace())
		{
			error("\tat " + element.toString());
		}
	}
	
	public static synchronized void fatal(Exception e)
	{
		fatal(e.toString());
		for (StackTraceElement element : e.getStackTrace())
		{
			fatal("\tat " + element.toString());
		}
	}
	
	private static synchronized void print(String message)
	{
		// construct the log message and log it
		String logMessage;
		if (printTimestamp)
		{
			logMessage = getCurrentTimeTag() + message;
		}
		else
		{
			logMessage = message;
		}
		if (outputToArea)
		{
			textArea.append(logMessage + "\n");
		}
		else
		{
			logStream.println(logMessage);
		}
		if (logToFile)
		{
			// if the log buffer is not full, store the message in the buffer
			if (count < getLogBufferSize() - 1)
			{
				logBuffer[count++] = logMessage;
			}
			// otherwise write the buffer to the log file
			else
			{
				String loggingToFileMessage = getCurrentTimeTag() + LogLevel.DEBUG.logLevel() + "Logging to file."; // construct a custom log message
				logStream.println(loggingToFileMessage);
				logBuffer[count] = loggingToFileMessage;
				writeLogBufferToFile();
				logBuffer = new String[logBufferSize];
				count = 0;
			}
		}
	}
	
	/**
	 * Returns the current time and date in the following format: {@code [Tue Dec 16 13:29:39 CET 2014]}.
	 * @return a date-time tag representing the current time and date
	 */
	private static String getCurrentTimeTag()
	{
		return "[" + new Date().toString() + "] ";
	}
	
	/**
	 * Publicly accessible method to write the current log buffer to the log file.
	 */
	public static void writeLogBufferToFile()
	{
		writeLogBufferToFile(logBuffer);
	}
	
	/**
	 * Writes the given string buffer to application.log.
	 * @param buffer - the {@code String} array to be written to the log file
	 */
	private static void writeLogBufferToFile(String[] buffer)
	{
		Writer out = null;
		try
		{
			out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("logs/application.log", true), "UTF-8"));
			for (String s : buffer)
			{
				if (s != null)
				{
					out.write(s + System.getProperty("line.separator"));
				}
			}
		}
		catch (IOException e)
		{
			error("There was an error logging to error.log: " + e.getMessage());
		}
		finally
		{
			try
			{
				out.close();
			}
			catch (IOException ignore)
			{
				// exception closing the stream, panic now!
				Log.fatal(ignore);
				System.exit(0);
			}
		}
	}
}

