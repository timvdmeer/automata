package main;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import exceptions.ParseException;
import exceptions.RegexException;
import exceptions.TooManyElementsException;
import regex.*;

public class Utility
{
	
	public static enum AUTOMATON_READER_STATE {INIT, STATES, ALPHABET, TRANSITION_FUNCTION, START_STATE, ACCEPT_STATES};
	
	private static final int MAX_AMOUNT_OF_POWERSET_ELEMENTS_TO_NOT_BLOW_UP_COMPUTER = 15;
	private static int newStateName = 1;
	
	/**
	 * Generates a state name that is guaranteed to be unique for this run of the application.
	 * @return a unique state name
	 */
	public static synchronized int getNewStateName()
	{
		return newStateName++;
	}

	/**
	 * Returns the power set of the given set of states.
	 * @param <T>
	 * @param setin - the set to make a power set from
	 * @return a set of sets of states equivalent to the power set of the given set
	 * @throws TooManyElementsException if the size of the given set is too large
	 */
	public static <T> Set<Set<T>> powerset(Set<T> setin) throws TooManyElementsException
	{	
		// make sure we do not take more time than the current age of the universe to compute 2^(size of given set) elements
		if (setin.size() > MAX_AMOUNT_OF_POWERSET_ELEMENTS_TO_NOT_BLOW_UP_COMPUTER)
		{
			throw new TooManyElementsException("Do not blow up computer. Too many elements in set!");
		}
		// construct an array for easier iteration over the elements of the given set
		List<T> originalSet = new ArrayList<>(setin);
		Set<Set<T>> powerset = new HashSet<>();
		
		// amount of elements that will be in the power set
		int powersetSize = (int)Math.pow(2, setin.size());
		
		// for each element in the power set
		for (int i=0; i<powersetSize; i++)
		{
			// create the power set element (which is another set)
			Set<T> powersetElement = new HashSet<>();
			
			// construct the binary representation of the current count i (which in turn represents the i-th element in the power set)
			String binaryString = Integer.toBinaryString(i);
			// pad with zeroes (we want char arrays of the form [001], [010] and [000] and not [1], [10] and [])
			while (binaryString.length() < setin.size())
			{
				binaryString = "0" + binaryString;
			}
			char[] binary = binaryString.toCharArray();
	
			// for each bit in the binary representation of count i
			for (int j=0; j<binary.length; j++)
			{
				// check whether the element with count j in the original set should be in this power set element
				if (binary[j] == '1')
				{
					// add it to this power set element
					powersetElement.add(originalSet.get(j));
				}
			}
			
			// finally add this power set element to the power set
			powerset.add(powersetElement);
		}
		
		// when all 2^n elements have been generated, return the full power set
		return powerset;
	}
	
	/**
	 * Parses the content models in a JSON file into a regex.
	 * @param path - the path to the file
	 * @return a list containing the roots of the AST's representing the regexes from the JSON file
	 * @throws IOException if an IO error occurred during the parsing of the JSON file
	 * @throws ParseException if the JSON does not represent a valid regex
	 * @throws IllegalStateException if an internal error in the GSON library occurs
	 * @throws RegexException if something went wrong while creating the AST's
	 */
	public static List<RegexElement> parseRegexFromJSONFile(String path) throws IOException, ParseException, IllegalStateException, RegexException
	{
		JsonObject json = new JsonParser().parse(new JsonReader(new BufferedReader(new FileReader(path)))).getAsJsonObject();
		
		JsonArray contentModels = (JsonArray) json.get("contentModels");;
		if (contentModels == null)
		{
			throw new ParseException("The root object in the specified JSON file does not have an attribute with the name 'contentModels'!");
		}
		if (json.get("attributes") == null) Log.warn("The attribute 'attributes' is missing from the JSON file!");
		if (json.get("elements") == null) Log.warn("The attribute 'elements' is missing from the JSON file!");
		
		// parse every content model into a regex and store it in a list
		List<RegexElement> regexList = new ArrayList<>();
		Iterator<JsonElement> iterator = contentModels.iterator();
		while (iterator.hasNext())
		{
			regexList.add(parseRegexFromJSON(iterator.next().getAsJsonObject()));
		}
		
		return regexList;
	}
	
	/**
	 * Parses a JSON object into an equivalent regex.
	 * @param json - the JSON object to be converted into a regex AST
	 * @return the root of the regex AST that is equivalent to the given JSON object
	 * @throws ParseException if the JSON does not represent a valid regex
	 * @throws IllegalStateException if an internal error in the GSON library occurs
	 * @throws RegexException if something went wrong while creating the AST
	 */
	private static RegexElement parseRegexFromJSON(JsonObject json) throws ParseException, RegexException
	{
		JsonElement minOccurs = json.get("minOccurs");
		JsonElement maxOccurs = json.get("maxOccurs");
		
		// minOccurs does not exist and maxOccurs does not exist
		if (minOccurs == null && maxOccurs == null)
		{
			throw new ParseException("Both minOccurs and maxOccurs are missing from an element!");
		}
		// minOccurs does not exist and maxOccurs = n
		if (minOccurs == null  && maxOccurs != null)
		{
			throw new ParseException("Cannot have a maxOccurs without a minOccurs!");					
		}
		
		// fix for "maxOccurs": null instead of maxOccurs not existing for unboundedness
		if (maxOccurs == JsonNull.INSTANCE)
		{
			maxOccurs = null;
		}
		
		// the element representing the JSON object
		RegexElement objectElement = null;
		
		switch (json.get("type").getAsString())
		{
			case "sequence":
				JsonArray concatenationItems = json.get("items").getAsJsonArray();
				List<RegexElement> concatenationElements = new ArrayList<>();
				// parse all objects in the array
				for (JsonElement element : concatenationItems)
				{
					concatenationElements.add(parseRegexFromJSON((JsonObject) element));
				}
				if (concatenationElements.isEmpty())
				{
					throw new ParseException("A sequence is empty!");
				}
				// if the list contains only element, there is no concatenation and we simply return the only element
				if (concatenationElements.size() == 1)
				{
					objectElement = concatenationElements.iterator().next();
				}
				else
				{
					objectElement = new Concatenation(concatenationElements);
				}
				break;
				
			case "choice":
				JsonArray choiceItems = json.get("items").getAsJsonArray();
				List<RegexElement> choiceElements = new ArrayList<>();
				
				// parse all elements of the choice
				for (JsonElement element : choiceItems)
				{
					choiceElements.add(parseRegexFromJSON((JsonObject) element));
				}
				if (choiceElements.isEmpty())
				{
					throw new ParseException("A choice is empty!");
				}
				// if the list contains only one element, there is no choice and we simply return the only element
				if (choiceElements.size() == 1)
				{
					objectElement = choiceElements.iterator().next();
				}
				else
				{
					objectElement = createUnion(choiceElements);
				}
				break;
				
			case "element":
				objectElement = new Symbol(json.get("localName").getAsString());
				break;
				
			case "empty":
				return new Symbol(Symbol.EPSILON);
	
			case "any":
				objectElement = new Symbol("%any");
				break;
				
			default:
				Log.debug(json.get("type").getAsString());
				throw new ParseException("Unknown JSON object type.");
		}
		
		// add multiplicities to the element
		List<RegexElement> concatenationElements = new ArrayList<>();
		if (minOccurs != null && maxOccurs != null)
		{
			int min = minOccurs.getAsInt();
			int max = maxOccurs.getAsInt();
			
			// bounds check
			if (max < min || max == 0)
			{
				throw new ParseException("Impossible bounds, either maxOccurs is smaller than minOccurs or both are zero!");
			}
			// repeat a minOccurs times
			for (int i=0; i<min; i++)
			{
				concatenationElements.add(objectElement);
			}
			// repeat (a|-) maxOccurs-minOccurs times
			for (int i=0; i<max-min; i++)
			{
				concatenationElements.add(new Union(objectElement, new Symbol(Symbol.EPSILON)));
			}
			// if the concatenation would only contain one element, there is no concatenation and we simply return its only element
			if (concatenationElements.size() == 1)
			{
				return concatenationElements.iterator().next();
			}
			return new Concatenation(concatenationElements);
		}
		if (minOccurs != null && maxOccurs == null)
		{
			int min = minOccurs.getAsInt();
			
			// if minOccurs is zero that means we return a* and there is no need for a concatenation
			if (min == 0)
			{
				return new Star(objectElement);
			}
			// repeat a minOccurs times
			for (int i=0; i<min; i++)
			{
				concatenationElements.add(objectElement);
			}
			// add a*
			concatenationElements.add(new Star(objectElement));
			return new Concatenation(concatenationElements);
		}
		
		throw new ParseException("Something absolutely terrible went wrong!");
	}
	
	/**
	 * Creates a tree of union elements from a list of elements that should be unified.
	 * @param unionElements - the list of elements
	 * @return a tree of unions formed from the elements from the given list
	 */
	private static Union createUnion(List<RegexElement> unionElements)
	{
		if (unionElements.size() == 2)
		{
			Iterator<RegexElement> iterator = unionElements.iterator();
			return new Union(iterator.next(), iterator.next());
		}
		RegexElement element = unionElements.iterator().next();
		unionElements.remove(element);
		return new Union(element, createUnion(unionElements));
	}
	
	/**
	 * Parses a regex from a file.
	 * @param path - the path to the file
	 * @param generateBinaryConcatenations - whether the algorithm should generate 'true' binary concatenations or is allowed
	 * to make life easier by merging several concatenations into a big list
	 * @return the root regex element
	 * @throws IOException if something went wrong while reading the file
	 * @throws ParseException whenever the regex inside the file is not a valid regex
	 */
	public static RegexElement parseRegexFromFile(String path, boolean generateBinaryConcatenations) throws IOException, ParseException
	{
		try (BufferedReader reader = new BufferedReader(new FileReader(path)))
		{
			String line;
			if ((line = reader.readLine()) != null)
			{
				Log.debug("Parsing regex:\n" + line + "\n");
				return parseRegexFromString(line, generateBinaryConcatenations);
			}
		}
		
		throw new ParseException("The file to be parsed was empty!");
	}
	
	/**
	 * Parses a regex from a string. Determines the root element of the string and then recursively parses the operands of that root element.
	 * @param string - the string to parse
	 * @param generateBinaryConcatenations - whether the algorithm should generate 'true' binary concatenations or is allowed
	 * to make life easier by merging several concatenations into a big list
	 * @return the root regex element representing the full regex contained in the string
	 * @throws ParseException whenever the string doesn't represent a well-formed regex
	 */
	private static RegexElement parseRegexFromString(String string, boolean generateBinaryConcatenations) throws ParseException
	{
		// note: this method relies heavily on the well-formedness of the regex!
		
		// remove parentheses
		if (string.startsWith("("))
		{
			int nestingLevel = 0;
			boolean a = true;
			for (int i=0; i<string.length(); i++)
			{
				if (string.charAt(i) == '(')
				{
					nestingLevel++;
				}
				if (string.charAt(i) == ')')
				{
					nestingLevel--;
					if (a && nestingLevel == 0 && i != string.length() - 1)
					{
						a = false;
						break;
					}
				}
			}
			if (nestingLevel == 0 && a)
			{
				// if the string starts with a parenthesis and the last element is a star, we already know that the star must be the root element
				if (string.charAt(string.length() - 1) == '*')
				{
					return new Star(parseRegexFromString(string.substring(1, string.length() - 2), generateBinaryConcatenations));
				}
				string = string.substring(1, string.length() - 1);
			}
		}
		
		ElementIndexPair root = findRootElement(string);

		switch (root.element)
		{
			case '*':
				// well-formedness check
				if (root.index == 0)
				{
					throw new ParseException("A star cannot be the first symbol in a string!");
				}
				return new Star(parseRegexFromString(string.substring(0, root.index), generateBinaryConcatenations));
				
			case '+':
				if (generateBinaryConcatenations)
				{
					return new BinaryConcatenation(parseRegexFromString(string.substring(0, root.index), generateBinaryConcatenations), parseRegexFromString(string.substring(root.index + 1, string.length()), generateBinaryConcatenations));
				}
				else
				{
					List<RegexElement> concatElements = new ArrayList<>();
					// add the string left of the root concatenation, this can be done safely without checking for other concatenations
					// left of the root node because the root node already has to be the left most non-nested concatenation
					concatElements.add(parseRegexFromString(string.substring(0, root.index), generateBinaryConcatenations));
					// for the string right of the root concatenation we search for more concatenations on the same nesting level, adding
					// the strings between them as additional concatenation list elements
					int nestingLevel = 0;
					int lastConcatElementIndex = root.index;
					for (int i=root.index+1; i<string.length(); i++)
					{
						// skip groups by checking for nesting level
						if (string.charAt(i) == '(')
						{
							nestingLevel++;
						}
						else if (string.charAt(i) == ')')
						{
							nestingLevel--;
						}
						if (string.charAt(i) == '+' && nestingLevel == 0)
						{
							concatElements.add(parseRegexFromString(string.substring(lastConcatElementIndex + 1, i), generateBinaryConcatenations));
							lastConcatElementIndex = i;
						}
					}
					// because the above algorithm adds strings LEFT of concatenation symbols it fails to include the string RIGHT of the
					// last concatenation, so we add it here
					if (lastConcatElementIndex != string.length() - 1)
					{
						concatElements.add(parseRegexFromString(string.substring(lastConcatElementIndex + 1, string.length()), generateBinaryConcatenations));
					}
					else
					{
						throw new ParseException("A concatenation cannot be the last symbol in a string!");
					}
					// basic check for parenthesis balance
					if (nestingLevel != 0)
					{
						throw new ParseException("The number of opening parentheses does not match the number of closing parentheses!");
					}
					try
					{
						return new Concatenation(concatElements);
					}
					catch (RegexException e)
					{
						throw new ParseException("While parsing an attempt was made to generate a concatenation with less than two elements!");
					}
				}
				
			case '|':
				// well-formedness check
				if (root.index == string.length() - 1)
				{
					throw new ParseException("A union cannot be the last symbol in a string!");
				}
				return new Union(parseRegexFromString(string.substring(0, root.index), generateBinaryConcatenations), parseRegexFromString(string.substring(root.index + 1, string.length()), generateBinaryConcatenations));

			case '-':
				return new Symbol(Symbol.EPSILON);
				
			default:
				return new Symbol(string.charAt(root.index));		
		}
	}
	
	/**
	 * Finds the root regex element in a string.
	 * @param string - the string to find the root element of
	 * @return an object containing both the char literal representing the root element and its index in the string
	 * @throws ParseException if the string is empty
	 */
	private static ElementIndexPair findRootElement(String string) throws ParseException
	{
		if (string.length() == 0)
		{
			throw new ParseException("Cannot parse an empty string!");
		}
		// if the string length is 1, only a symbol is a valid regex element
		if (string.length() == 1)
		{
			if (!isOperator(string.charAt(0)))
			{
				return new ElementIndexPair(string.charAt(0), 0);
			}
			throw new ParseException("An operator without operand is not a valid regex!");
		}
		// the first occurrence of the weakest binding operator that is not in a group is the root element
		ElementIndexPair weakestBindingOperator = new ElementIndexPair(string.charAt(0), 0);
		boolean firstOperator = true;
		int nestingLevel = 0;
		for (int i=0; i<string.length(); i++)
		{
			// skip groups by checking for nesting level
			if (string.charAt(i) == '(')
			{
				nestingLevel++;
			}
			else if (string.charAt(i) == ')')
			{
				nestingLevel--;
			}
			if (isOperator(string.charAt(i)) && nestingLevel == 0)
			{
				if (firstOperator)
				{
					firstOperator = false;
					weakestBindingOperator = new ElementIndexPair(string.charAt(i), i);
				}
				if (isWeakerBindingOperatorThan(string.charAt(i), weakestBindingOperator.element))
				{
					weakestBindingOperator = new ElementIndexPair(string.charAt(i), i);
				}
			}
		}
		
		return weakestBindingOperator;
	}
	
	/**
	 * Determines whether a given character is a valid regex operator.
	 * @param c - the character to check
	 * @return true if the character is a regex operator, false otherwise
	 */
	private static boolean isOperator(char c)
	{
		return (c == '*' || c == '|' || c == '+');
	}
	
	/**
	 * Compares two regex operators by their binding strength (in this order: star > concatenation > union)
	 * @param a - the first operator
	 * @param b - the second operator
	 * @return true if a is weaker than b, false otherwise
	 */
	private static boolean isWeakerBindingOperatorThan(char a, char b)
	{
		return (a == '|' && (b == '+' || b == '*')) | (a == '+' && b == '*');
	}
	
	/**
	 * A simple pair class representing a regex element and the index at which it is found in a string.
	 */
	private static class ElementIndexPair
	{
		char element;
		int index;
		
		private ElementIndexPair(char element, int index)
		{
			this.element = element;
			this.index = index;
		}
	}
	
	/**
	 * Computes the symmetric difference of two sets, i.e. all elements not in the intersection of both sets.
	 * @param set1 - the first set
	 * @param set2 - the second set
	 * @return a set containing all elements from both sets that are not in the intersection of both sets
	 */
	public static <T> Set<T> symmetricDifference(Set<T> set1, Set<T> set2)
	{
		Set<T> symmetricDifference = new HashSet<>();
		
		boolean elementInBothSets = false;
		for (T element1 : set1)
		{
			for (T element2 : set2)
			{
				if (element1.equals(element2))
				{
					elementInBothSets = true;
					break;
				}
			}
			if (!elementInBothSets)
			{
				symmetricDifference.add(element1);
			}
			elementInBothSets = false;
		}
		
		return symmetricDifference;
	}
	
	/**
	 * Computes the intersection of two sets, i.e. all elements that are in both sets.
	 * @param set1 - the first set
	 * @param set2 - the second set
	 * @return a set containing all elements that are in both sets
	 */
	public static <T> Set<T> intersection(Set<T> set1, Set<T> set2)
	{
		Set<T> intersection = new HashSet<>();
		
		for (T element1 : set1)
		{
			for (T element2 : set2)
			{
				if (element1.equals(element2))
				{
					intersection.add(element1);
				}
			}
		}
		
		return intersection;
	}
	
	/**
	 * Checks whether the given element is present in a set. Uses the element's equal method to determine presence.
	 * @param set1 - the set to check
	 * @param element - the element to check for
	 * @return true if the set contains the element, false otherwise
	 */
	public static <T> boolean contains(Set<T> set1, T element)
	{
		for (T setElement : set1)
		{
			if (setElement.equals(element))
			{
				return true;
			}
		}
		
		return false;
	}
}
