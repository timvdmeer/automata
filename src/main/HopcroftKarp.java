package main;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Algorithm due to:
 * Hopcroft, John E.; Karp, Richard M. (1973), "An n5/2 algorithm for maximum matchings in bipartite graphs", 
 * SIAM Journal on Computing 2 (4): 225�231.
 */
public class HopcroftKarp
{
	
	
	
	/**
	 * Computes the maximum matching for the given unweighted undirected bipartite graph.
	 */
	public static Set<Edge> maximumMatchingHopcroftKarp(BipartiteGraph graph)
	{
		// M = empty
		Set<Edge> maximumMatching = new HashSet<>();
		// P = empty
		Set<Edge> augmentingPaths = new HashSet<>();
		
		do
		{
			// P = augmenting paths relative to M
			augmentingPaths = augmentingPaths();
			// M = symmetric difference of M and P
			maximumMatching = symmetricDifference(maximumMatching, augmentingPaths);
		}
		while (true == false); // until P = empty
		
		return maximumMatching;
	}
	
	public static <T> Set<T> augmentingPaths()
	{
		Set<T> augmentingPath = new HashSet<>();
		
		
		
		return augmentingPath;
	}
	
	/**
	 * Computes the symmetric difference of two sets, i.e. all elements not in the intersection of both sets.
	 * @param set1 - the first set
	 * @param set2 - the second set
	 * @return a set containing all elements from both sets that are not in the intersection of both sets
	 */
	private static <T> Set<T> symmetricDifference(Set<T> set1, Set<T> set2)
	{
		Set<T> symmetricDifference = new HashSet<>();
		
		boolean elementInBothSets = false;
		for (T element1 : set1)
		{
			for (T element2 : set2)
			{
				if (element1.equals(element2))
				{
					elementInBothSets = true;
					break;
				}
			}
			if (!elementInBothSets)
			{
				symmetricDifference.add(element1);
			}
			elementInBothSets = false;
		}
		
		return symmetricDifference;
	}
	
	
	}
	
//	public static List<Integer> getMatching(List<Integer[]> gU, int gVCount)
//	{
//		gU = new LinkedList<>(gU);
//		gU.add(new Integer[] {});
//		
//		int iNil = gU.size();
//		int gUCount = iNil;
//		
//		List<Integer> pairU = new LinkedList<>();
//		for (int i=0; i<gUCount; i++)
//		{
//			pairU.add(iNil);
//		}
//		List<Integer> pairV = new LinkedList<>();
//		for (int i=0; i<gVCount; i++)
//		{
//			pairU.add(iNil);
//		}
//		
//		int[] dist = new int[gU.size()];
//		int[] q = new int[gU.size()];
//		
//		while(bfs(gU, pairU, pairV, dist, iNil, gUCount, q))
//		{
//			for (int u=0; u<gUCount; ++u)
//			{
//				if (pairU.get(u) == iNil)
//				{
//					dfs(gU, pairU, pairV, dist, iNil, u);
//				}
//			}
//		}
//		
//		return pairU.select(i => i == iNil ? -1 : i).take(gUCount).toList();
//	}
//	
//	private static boolean dfs(List<Integer[]> gU, List<Integer> pairU, List<Integer> pairV, int[] dist, int iNil, int u)
//	{
//		return false;
//	}
//
//	private static boolean bfs(List<Integer[]> gU, List<Integer> pairU, List<Integer> pairV, int[] dist, int iNil, int gUCount, int[] q)
//	{
//		return false;
//	}
	
	
	// --------------------------------------------------------------------------------------
	
	
//	public final int NIL = 0;
//
//    public final int INF = Integer.MAX_VALUE;
//
//    public ArrayList<Integer>[] Adj; 
//
//    public int[] Pair;
//
//    public int[] Dist;
//
//    public int cx, cy;
//
// 
//
//     /** Function BFS **/
//
//    public boolean BFS() 
//
//    {
//
//        Queue<Integer> queue = new LinkedList<Integer>();
//
//        for (int v = 1; v <= cx; ++v) 
//
//            if (Pair[v] == NIL) 
//
//            { 
//
//                Dist[v] = 0; 
//
//                queue.add(v); 
//
//            }
//
//            else 
//
//                Dist[v] = INF;
//
// 
//
//        Dist[NIL] = INF;
//
// 
//
//        while (!queue.isEmpty()) 
//
//        {
//
//            int v = queue.poll();
//
//            if (Dist[v] < Dist[NIL]) 
//
//                for (int u : Adj[v]) 
//
//                    if (Dist[Pair[u]] == INF) 
//
//                    {
//
//                        Dist[Pair[u]] = Dist[v] + 1;
//
//                        queue.add(Pair[u]);
//
//                    }           
//
//        }
//
//        return Dist[NIL] != INF;
//
//    }    
//
//     /** Function DFS **/
//
//    public boolean DFS(int v) 
//
//    {
//
//        if (v != NIL) 
//
//        {
//
//            for (int u : Adj[v]) 
//
//                if (Dist[Pair[u]] == Dist[v] + 1)
//
//                    if (DFS(Pair[u])) 
//
//                    {
//
//                        Pair[u] = v;
//
//                        Pair[v] = u;
//
//                        return true;
//
//                    }               
//
// 
//
//            Dist[v] = INF;
//
//            return false;
//
//        }
//
//        return true;
//
//    }
//
//     /** Function to get maximum matching **/
//
//    public int HopcroftKarp() 
//
//    {
//
//        Pair = new int[cx + cy + 1];
//
//        Dist = new int[cx + cy + 1];
//
//        int matching = 0;
//
//        while (BFS())
//
//            for (int v = 1; v <= cx; ++v)
//
//                if (Pair[v] == NIL)
//
//                    if (DFS(v))
//
//                        matching = matching + 1;
//
//        return matching;
//
//    }
//
//    /** Function to make graph with vertices x , y **/
//
//    public void makeGraph(int[] x, int[] y, int E)
//
//    {
//
//        Adj = new ArrayList[cx + cy + 1];
//
//        for (int i = 0; i < Adj.length; ++i)
//
//            Adj[i] = new ArrayList<Integer>();        
//
//        /** adding edges **/    
//
//        for (int i = 0; i < E; ++i) 
//
//            addEdge(x[i] + 1, y[i] + 1);    
//
//    }
//
//    /** Function to add a edge **/
//
//    public void addEdge(int u, int v) 
//
//    {
//
//        Adj[u].add(cx + v);
//
//        Adj[cx + v].add(u);
//
//    } 
