package main;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class BipartiteGraph
{
	private static final int NIL = -1;
	private static final int INF = Integer.MAX_VALUE;
	
	private Set<Integer> U;
	private Set<Integer> V;
	private Set<Edge> E;
	
	public BipartiteGraph(Set<Integer> U, Set<Integer> V, Set<Edge> E)
	{
		this.U = U;
		this.V = V;
		this.E = E;
	}
	
	public int maximumMatching()
	{
		
		
		return -1;
	}
	
	private boolean bfs()
	{
		return false;
	}
	
	private boolean dfs()
	{
		return false;
	}
	
	public Set<Integer> U()
	{
		return U;
	}
	
	public Set<Integer> V()
	{
		return V;
	}
	
	public Set<Edge> E()
	{
		return E;
	}
}
