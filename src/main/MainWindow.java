package main;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.border.EtchedBorder;
import automata.DFA;
import automata.NFA;
import exceptions.DuplicateTransitionException;
import exceptions.IllFormedAutomatonException;
import exceptions.ParseException;
import exceptions.RegexException;
import exceptions.StateException;
import regex.RegexElement;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.JTextArea;

public class MainWindow {

	private JFrame frame;
	private JTextField txtChooseFile;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}
	
	private void initLog(JTextArea textArea)
	{
		Log.setLogToFile(false);
		Log.setOutputToFrame(textArea);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setMinimumSize(new Dimension(1250, 800));
		frame.setMaximumSize(new Dimension(1250, 800));
		frame.setResizable(false);
		frame.setSize(new Dimension(1250, 800));
		frame.setPreferredSize(new Dimension(1250, 800));
		frame.setBounds(100, 100, 1140, 777);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		tabbedPane.setBounds(10, 11, 1224, 580);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		tabbedPane.addTab("New tab", null, panel, null);
		
		JPanel pnlChooseFile = new JPanel();
		pnlChooseFile.setBounds(10, 11, 1199, 68);
		pnlChooseFile.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlChooseFile.setLayout(null);
		
		txtChooseFile = new JTextField();
		txtChooseFile.setBounds(10, 36, 1013, 20);
		pnlChooseFile.add(txtChooseFile);
		txtChooseFile.setColumns(10);
		
		JButton btnChooseFile = new JButton("Choose File");
		btnChooseFile.setBounds(1033, 35, 87, 23);
		pnlChooseFile.add(btnChooseFile);
		
		JLabel lblChooseFile = new JLabel("Choose a JSON file:");
		lblChooseFile.setBounds(10, 11, 95, 14);
		pnlChooseFile.add(lblChooseFile);
		
		JPanel pnlRegularExpressions = new JPanel();
		pnlRegularExpressions.setBounds(10, 90, 751, 451);
		pnlRegularExpressions.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JScrollPane scrRegularExpressions = new JScrollPane();
		scrRegularExpressions.setBounds(10, 36, 731, 404);
		pnlRegularExpressions.add(scrRegularExpressions);
		
		JList<RegexElement> lstRegularExpressions = new JList<>();
		lstRegularExpressions.setFont(new Font("Courier New", Font.PLAIN, 12));
		scrRegularExpressions.setViewportView(lstRegularExpressions);
		lstRegularExpressions.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lstRegularExpressions.setVisibleRowCount(-1);
		lstRegularExpressions.setBorder(null);
		pnlRegularExpressions.setLayout(null);
		
		JLabel lblRegularExpressions = new JLabel("Regular expressions corresponding to the content models:");
		lblRegularExpressions.setBounds(10, 11, 280, 14);
		pnlRegularExpressions.add(lblRegularExpressions);
		panel.setLayout(null);
		
		JButton btnParseRegex = new JButton("Parse");
		btnParseRegex.setBounds(1130, 35, 59, 23);
		pnlChooseFile.add(btnParseRegex);
		panel.add(pnlChooseFile);
		panel.add(pnlRegularExpressions);
		
		JPanel pnlMinimizedDFA = new JPanel();
		pnlMinimizedDFA.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlMinimizedDFA.setBounds(771, 90, 438, 451);
		panel.add(pnlMinimizedDFA);
		pnlMinimizedDFA.setLayout(null);
		
		JLabel lblMinimizedDfa = new JLabel("Minimized DFA:");
		lblMinimizedDfa.setBounds(10, 11, 72, 14);
		pnlMinimizedDFA.add(lblMinimizedDfa);
		
		JScrollPane scrMinimizedDFA = new JScrollPane();
		scrMinimizedDFA.setBounds(10, 36, 418, 404);
		pnlMinimizedDFA.add(scrMinimizedDFA);
		
		JTextArea txtAreaMinimizedDFA = new JTextArea();
		txtAreaMinimizedDFA.setEditable(false);
		scrMinimizedDFA.setViewportView(txtAreaMinimizedDFA);
		frame.getContentPane().add(tabbedPane);
		
		JPanel pnlConsole = new JPanel();
		pnlConsole.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlConsole.setBounds(10, 602, 1224, 158);
		frame.getContentPane().add(pnlConsole);
		pnlConsole.setLayout(null);
		
		JScrollPane scrConsole = new JScrollPane();
		scrConsole.setBounds(0, 0, 1224, 158);
		pnlConsole.add(scrConsole);
		
		JTextArea txtAreaConsole = new JTextArea();
		txtAreaConsole.setEditable(false);
		scrConsole.setViewportView(txtAreaConsole);
		btnParseRegex.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				txtAreaConsole.setText("");
				if (txtChooseFile.getText().isEmpty())
				{
					Log.warn("Choose a file before trying to parse.");
				}
				else
				{
					List<RegexElement> list = null;
					try
					{
						list = Utility.parseRegexFromJSONFile(txtChooseFile.getText());
						DefaultListModel<RegexElement> listModel = new DefaultListModel<>();
						for (RegexElement regex : list)
						{
							listModel.addElement(regex);
						}
						lstRegularExpressions.setModel(listModel);
					}
					catch (IllegalStateException | IOException | ParseException | RegexException e1)
					{
						Log.error(e1);
					}
				}
			}
		});
		
		btnChooseFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtAreaConsole.setText("");
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.showOpenDialog(tabbedPane);
				File file = fileChooser.getSelectedFile();
				if (file != null)
				{
					txtChooseFile.setText(file.getAbsolutePath());
				}
			}
		});
		
		
		JButton btnOptimizeSelectedExpression = new JButton("Optimize Selected Expression");
		btnOptimizeSelectedExpression.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtAreaMinimizedDFA.setText("");
				txtAreaConsole.setText("");
				int selectedIndex = lstRegularExpressions.getSelectedIndex();
				if (selectedIndex == -1)
				{
					Log.warn("You must select an expression first.");
				}
				else
				{
					RegexElement regex = lstRegularExpressions.getSelectedValue();
					try
					{
						NFA nfa = regex.convertToNFA();
						Log.debug(nfa.toString());
						DFA dfa = nfa.convertToDFA();
						Log.debug(dfa.toString());
						DFA minimizedDFA = dfa.minimize(true);
						Log.debug("[MINIMIZED DFA]");
						Log.debug(minimizedDFA.toString());
						//Log.debug(minimizedDFA.convertToRegex().toString());
						
						txtAreaMinimizedDFA.setText("");
						txtAreaMinimizedDFA.append(minimizedDFA.toString());
					}
					catch (RegexException | IllFormedAutomatonException | DuplicateTransitionException | StateException e1)
					{
						Log.error(e1);
					}
				}
			}
		});
		btnOptimizeSelectedExpression.setBounds(568, 7, 173, 23);
		pnlRegularExpressions.add(btnOptimizeSelectedExpression);
		
		initLog(txtAreaConsole);
	}
}
