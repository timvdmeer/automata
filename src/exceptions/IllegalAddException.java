package exceptions;

@SuppressWarnings("serial")
public class IllegalAddException extends Exception
{
	public IllegalAddException(String message)
	{
		super(message);
	}
}
