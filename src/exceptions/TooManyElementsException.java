package exceptions;
@SuppressWarnings("serial")
public class TooManyElementsException extends Exception
{
	public TooManyElementsException(String message)
	{
		super(message);
	}
}
