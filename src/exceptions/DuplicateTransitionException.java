package exceptions;
@SuppressWarnings("serial")
public class DuplicateTransitionException extends Exception
{
	public DuplicateTransitionException(String message)
	{
		super(message);
	}
}
