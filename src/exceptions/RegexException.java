package exceptions;

@SuppressWarnings("serial")
public class RegexException extends Exception
{
	public RegexException(String message)
	{
		super(message);
	}
}
