package exceptions;

@SuppressWarnings("serial")
public class IllFormedAutomatonException extends Exception
{
	public IllFormedAutomatonException(String message)
	{
		super(message);
	}
}
