package automata;

public class Transition
{
	private State originState;
	private String symbol;
	private State newState;
	
	public Transition(State originState, String symbol, State newState)
	{
		this.originState = originState;
		this.symbol = symbol;
		this.newState = newState;
	}
	
	/**
	 * Compares this transition with the given transition. Two transitions are 'duplicate' (with respect to a DFA) if the current state and symbol are the same.
	 * @param transition - the transition to be compared to
	 * @return true if the current state and symbol are the same, otherwise false
	 */
	public boolean isDuplicateTransition(Transition transition)
	{
		if (originState.getName().equals(transition.getOriginState().getName()) && symbol.equals(transition.getSymbol()))
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Compares this transition with the given transition. Two transitions are equal if their origin state, new state and symbols match.
	 * @param transition - the transition to be compared to
	 * @return true if the origin state, new state and symbols of both transitions are the same, otherwise false
	 */
	public boolean isEqualTo(Transition transition)
	{
		return (originState.getName().equals(transition.getOriginState().getName()) &&
				symbol.equals(transition.getSymbol()) &&
				newState.getName().equals(transition.getNewState().getName()));
	}
	
	public State getOriginState()
	{
		return originState;
	}
	
	public State getNewState()
	{
		return newState;
	}
	
	public String getSymbol()
	{
		return symbol;
	}
	
	/**
	 * Returns a string of the form (state,symbol) = state. Example:
	 * (1,a) = 3
	 */
	public String toString()
	{
		return "(" + originState.getName() + "," + symbol + ") = " + newState.getName();
	}
}
