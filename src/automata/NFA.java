package automata;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import exceptions.DuplicateTransitionException;
import exceptions.RegexException;
import exceptions.StateException;
import main.Log;
import main.Utility;
import main.Utility.AUTOMATON_READER_STATE;
import regex.*;

public class NFA
{
	private Set<State> states;
	private Set<String> alphabet;
	private Set<Transition> transitionFunction;
	private State startState;
	private Set<State> acceptStates;
	
	/**
	 * Constructs a NFA from the given parameters. Accepts sets.
	 * @param states - the set of states
	 * @param alphabet - the set of symbols (strings) forming the alphabet
	 * @param transitionFunction - the set of transitions
	 * @param startState - the start state
	 * @param acceptStates - the set of accept states
	 * @return a NFA with the given parameters
	 */
	public NFA(Set<State> states, Set<String> alphabet, Set<Transition> transitionFunction, State startState, Set<State> acceptStates)
	{
		this.states = states;
		this.alphabet = alphabet;
		this.alphabet.add(Symbol.EPSILON); // add the empty string to the alphabet since this is a NFA
		this.transitionFunction = transitionFunction;
		this.startState = startState;
		this.acceptStates = acceptStates;
	}

	/**
	 * Constructs a NFA from the given parameters. Accepts arrays.
	 * @param states - the set of states
	 * @param alphabet - the set of symbols (strings) forming the alphabet
	 * @param transitionFunction - the set of transitions
	 * @param startState - the start state
	 * @param acceptStates - the set of accept states
	 * @return a NFA with the given parameters
	 */
	public static NFA createNFA(State[] states, String[] alphabet, Transition[] transitionFunction, State startState, State[] acceptStates)
	{
		Set<State> newStates = new HashSet<>(states.length);
		for (State state : states)
		{
			newStates.add(state);
		}
		Set<String> newAlphabet = new HashSet<>(alphabet.length);
		for (String symbol : alphabet)
		{
			newAlphabet.add(symbol);
		}
		Set<Transition> newTransitionFunction = new HashSet<>(transitionFunction.length);
		for (Transition transition : transitionFunction)
		{
			newTransitionFunction.add(transition);
		}
		// start state can be directly copied from the parameters of this method
		Set<State> newAcceptStates = new HashSet<>(acceptStates.length);
		for (State state : acceptStates)
		{
			newAcceptStates.add(state);
		}
		return new NFA(newStates, newAlphabet, newTransitionFunction, startState, newAcceptStates);
	}
	
	/**
	 * Constructs a NFA from a file.
	 * @param path - the path to the file
	 * @return a NFA
	 * @throws IOException an IOException is thrown whenever there is an error in reading the NFA from the file
	 */
	public static NFA createNFA(String path) throws IOException
	{
		Set<State> states = new HashSet<>();
		Set<String> alphabet = new HashSet<>();
		Set<Transition> transitionFunction = new HashSet<>();
		State startState = null;
		Set<State> acceptStates = new HashSet<>();	
		
		try (BufferedReader reader = new BufferedReader(new FileReader(path)))
		{
			String line;
			AUTOMATON_READER_STATE readerState = AUTOMATON_READER_STATE.INIT;
			while ((line = reader.readLine()) != null)
			{
				// if the line is a comment
				if (line.startsWith("#"))
				{
					continue;
				}
				switch (line)
				{
					case "[States]":
						if (readerState == AUTOMATON_READER_STATE.INIT)
						{
							readerState = AUTOMATON_READER_STATE.STATES;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Alphabet]":
						if (readerState == AUTOMATON_READER_STATE.STATES)
						{
							readerState = AUTOMATON_READER_STATE.ALPHABET;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Transition Function]":
						if (readerState == AUTOMATON_READER_STATE.ALPHABET)
						{
							readerState = AUTOMATON_READER_STATE.TRANSITION_FUNCTION;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Start State]":
						if (readerState == AUTOMATON_READER_STATE.TRANSITION_FUNCTION)
						{
							readerState = AUTOMATON_READER_STATE.START_STATE;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Accept States]":
						if (readerState == AUTOMATON_READER_STATE.START_STATE)
						{
							readerState = AUTOMATON_READER_STATE.ACCEPT_STATES;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					default:
						// continue with the next line if this one is empty
						if (line.isEmpty())
						{
							break;
						}
						switch (readerState)
						{
							case INIT:
								throw new IOException("Error reading file: current line has no label!");
								
							case STATES:
								states.add(new State(line));
								break;
								
							case ALPHABET:
								alphabet.add(line);
								break;
								
							case TRANSITION_FUNCTION:
								// remove all unnecessary characters from the line
								line = line.replaceAll("[()]", "");
								// split the line into its components, index 0 is the origin state, index 1 is the symbol and index 2 is the new state
								String[] parameters = line.split("[=,]");
								// if the length of the parameter array is not equal to 3 something has gone wrong
								if (parameters.length != 3)
								{
									throw new IOException("Error reading file: transition has not been defined correctly! Use the format: (state,symbol)=state");
								}
								// remove any leading and trailing white space from the parameters
								for (int i=0; i<parameters.length; i++)
								{
									if (parameters[i].charAt(0) == ' ')
									{
										parameters[i] = parameters[i].substring(1);
									}
									if (parameters[i].charAt(parameters[i].length() - 1) == ' ')
									{
										parameters[i] = parameters[i].substring(0, parameters[i].length() - 1);
									}
								}		
								// find the state objects belonging to this transition
								State originState = null;
								State newState = null;
								for (State state : states)
								{
									// if the state is equal to the origin state
									if (state.getName().equals(parameters[0]))
									{
										originState = state;
									}
									// if the state is equal to the new state
									if (state.getName().equals(parameters[2]))
									{
										newState = state;
									}
								}
								// if one of the states speficied in the transition was not found in the set of states, throw an exception
								if (originState == null || newState == null)
								{
									throw new IOException("Error reading file: a state in a transition does not match any state in the state set! Parameters are: [0]= " + parameters[0] + " [1]= " + parameters[1] + " [2]= " + parameters[2]);
								}
//								Transition transition = new Transition(originState, parameters[1], newState);
//								System.out.println(transition);
								transitionFunction.add(new Transition(originState, parameters[1], newState));
								break;
							
							case START_STATE:
								State start = null;
								for (State state : states)
								{
									if (state.getName().equals(line))
									{
										start = state;
										break;
									}
								}
								if (start == null)
								{
									throw new IOException("Error reading file: the start state did not match any state in the state set!");
								}
								startState = start;
								break;
							
							case ACCEPT_STATES:
								State acceptState = null;
								for (State state : states)
								{
									if (state.getName().equals(line))
									{
										acceptState = state;
									}
								}
								if (acceptState == null)
								{
									throw new IOException("Error reading file: one of the accept states did not match any state in the state set!");
								}
								acceptStates.add(acceptState);
								break;
						}
						break;
				}
			}
		}
		
		return new NFA(states, alphabet, transitionFunction, startState, acceptStates);
	}
	
	/**
	 * Does not work.
	 */
	@Deprecated
	public static NFA createGlushkovAutomaton(RegexElement regex) throws RegexException
	{
		markPositions(regex, 0); // mark each symbol in the regex with its position
		assignVariablesToRegexNodes(regex); // then assign the variables nullable, first and last to each node and update the follow set
		
		Set<State> states = new HashSet<>();
		Set<String> alphabet = new HashSet<>();
		Set<Transition> transitionFunction = new HashSet<>();
		State startState = null;
		Set<State> acceptStates = new HashSet<>();
		
		// the set of states of the automaton is the set of positions plus a new start state
		List<Symbol> positions = regex.getSymbols();
		for (Symbol position : positions)
		{
			// generate the alphabet
			alphabet.add(position.getSymbol());
			// and add the state corresponding to this position
			states.add(new State(position.toString()));
		}
		startState = new State("start");
		states.add(startState);
		
		for (Symbol position : regex.getLast())
		{
			for (State state : states)
			{
				if (state.getName().equals(position.toString()))
				{
					acceptStates.add(state);
				}
			}
		}
		
		// add transitions from start state
		for (String symbol : alphabet)
		{
			// (start,symbol)={x|x element of first(E), symbol(x) = symbol}
			State newState = null;
			Set<Symbol> firstE = regex.getFirst();
			for (Symbol position : firstE)
			{
				if (position.getSymbol().equals(symbol))
				{
					newState = findStateCorrespondingToPosition(position, states);
//					for (State state : states)
//					{
//						if (state.getName().equals(position.toString()))
//						{
//							newState = state;
//						}
//					}
				}
			}
			
			Transition transition = new Transition(startState, symbol, newState);
			transitionFunction.add(transition);
		}
		
		// for x element of pos(E), a element of alphabet, let (x,a)={y|y element of follow(E,x),symbol of y = a}
		for (Symbol x : positions)
		{
			for (String a : alphabet)
			{
				State originState = findStateCorrespondingToPosition(x, states);
				for (Symbol y : x.getFollow())
				{
					if (y.getSymbol().equals(a))
					{
						State newState = findStateCorrespondingToPosition(y, states);
						transitionFunction.add(new Transition(originState, a, newState));
					}
				}
			}
		}
		
		//TODO
		//if (epsilon part of language(E))
		{
			acceptStates.add(startState);
		}
		Set<Symbol> lastE = regex.getLast();
		Log.debug("----");
		for (Symbol position : lastE)
		{
			Log.debug(position.toString());
			acceptStates.add(findStateCorrespondingToPosition(position, states));
		}
		Log.debug("----");
		
		return new NFA(states, alphabet, transitionFunction, startState, acceptStates);
	}
	
	@Deprecated
	private static State findStateCorrespondingToPosition(Symbol position, Set<State> states)
	{
		for (State state : states)
		{
			if (state.getName().equals(position.toString()))
			{
				return state;
			}
		}
		
		return null;
	}
	
	/**
	 * Helper method for creating the Glushkov automaton. Traverses a regex tree in order and marks each symbol
	 * in the tree with its position in the regex.
	 * @param node - the regex to traverse
	 * @param position - the position to start incrementing from
	 * @return an integer whose value represents the next position
	 */
	@Deprecated
	private static int markPositions(RegexElement node, int position)
	{
		if (node == null)
		{
			return position;
		}
		// depending on the type of node, traverse its children
		if (node instanceof Symbol)
		{
			// symbol has no children so we do not traverse anything, instead we mark it with its position in the regex
			((Symbol) node).setPosition(++position);
			return position;
		}
		if (node instanceof Union)
		{
			// union always has two children, traverse left first and then right
			position = markPositions(((Union) node).getLeft(), position);
			position = markPositions(((Union) node).getRight(), position);
			return position;
		}
		if (node instanceof Star)
		{
			// star only has one child
			position = markPositions(((Star) node).getOperand(), position);
			return position;
		}
		if (node instanceof BinaryConcatenation)
		{
			// a concatenation has two, traverse left first and then right
			position = markPositions(((BinaryConcatenation) node).getLeft(), position);
			position = markPositions(((BinaryConcatenation) node).getRight(), position);
			return position;
		}
		return position;
	}
	
	/**
	 * Helper method for creating the Glushkov automaton. Traverses a regex tree in post order and assigns variables
	 * first, last and nullable to each node including follow for symbol nodes.
	 * @param node - the regex to traverse
	 */
	@Deprecated
	private static void assignVariablesToRegexNodes(RegexElement node) throws RegexException
	{
		if (node == null)
		{
			return;
		}
		// depending on the type of node, traverse its children
		if (node instanceof Symbol)
		{
			// symbol has no children so we do not traverse anything
		}
		if (node instanceof Union)
		{
			// union always has two children, traverse left first and then right
			assignVariablesToRegexNodes(((Union) node).getLeft());
			assignVariablesToRegexNodes(((Union) node).getRight());
		}
		if (node instanceof Star)
		{
			// star only has one child
			assignVariablesToRegexNodes(((Star) node).getOperand());
		}
		if (node instanceof BinaryConcatenation)
		{
			// a concatenation has two, traverse left first and then right
			assignVariablesToRegexNodes(((BinaryConcatenation) node).getLeft());
			assignVariablesToRegexNodes(((BinaryConcatenation) node).getRight());
		}
		assignVariablesToRegexNode(node); // always visit the node itself last
	}
	
	/**
	 * Helper method for creating the Glushkov automaton. Calculates the variables first, last and nullable for the given node.
	 * @param node - the regex node to visit
	 */
	@Deprecated
	private static void assignVariablesToRegexNode(RegexElement node) throws RegexException
	{
		if (node instanceof Symbol)
		{
			Symbol symbol = (Symbol) node;
			if (symbol.isEmptyLanguage())
			{
				// nullable(node) = false
				symbol.setNullable(false);
				// first(node) = empty
				symbol.setFirst(new HashSet<>());
				// last(node) = empty
				symbol.setLast(new HashSet<>());
			}
			else if (symbol.isEpsilon())
			{
				// nullable(node) = true
				symbol.setNullable(true);
				// first(node) = empty
				symbol.setFirst(new HashSet<>());
				// last(node) = empty
				symbol.setLast(new HashSet<>());
			}
			else
			{
				// nullable(xnode) = false;
				symbol.setNullable(false);
				// follow(x) = empty
				symbol.setFollow(new HashSet<>());
				// first(node) = {x}
				Set<Symbol> first = new HashSet<>(); 
				first.add(symbol);
				symbol.setFirst(first);
				// last(node) = {x}
				Set<Symbol> last = new HashSet<>();
				last.add(symbol);
				symbol.setLast(last);
			}
			return;
		}
		if (node instanceof Union)
		{
			// nullable(node) = nullable(left) or nullable(right)
			node.setNullable(((Union) node).getLeft().getNullable() || ((Union) node).getRight().getNullable());
			// first(node) = first(left) union first(right)
			Set<Symbol> first = new HashSet<>();
			first.addAll(((Union) node).getLeft().getFirst());
			first.addAll(((Union) node).getRight().getFirst());
			node.setFirst(first);
			// last(node) = last(left) union last(right)
			Set<Symbol> last = new HashSet<>();
			first.addAll(((Union) node).getLeft().getLast());
			first.addAll(((Union) node).getRight().getLast());
			node.setLast(last);
			return;
		}
		if (node instanceof BinaryConcatenation)
		{
			// nullable(node) = nullable(left) and nullable(right)
			node.setNullable(((BinaryConcatenation) node).getLeft().getNullable() && ((BinaryConcatenation) node).getRight().getNullable());
			// for each x in last(left) do
			for (Symbol x : ((BinaryConcatenation) node).getLeft().getLast())
			{
				// follow(x) = follow(x) union first(right)
				Set<Symbol> follow = new HashSet<>();
				follow.addAll(x.getFollow());
				follow.addAll(((BinaryConcatenation) node).getRight().getFirst());
				x.setFollow(follow);
			}
			// if nullable(left) then
			if (((BinaryConcatenation) node).getLeft().getNullable())
			{
				// first(node = first(left) union first(right)
				Set<Symbol> first = new HashSet<>();
				first.addAll(((BinaryConcatenation) node).getLeft().getFirst());
				first.addAll(((BinaryConcatenation) node).getRight().getFirst());
				node.setFirst(first);
			}
			else
			{
				// first(node) = first(left)
				node.setFirst(((BinaryConcatenation) node).getLeft().getFirst());
			}
			// if nullable(right) then
			if (((BinaryConcatenation) node).getRight().getNullable())
			{
				// last(node) = last(left) union last(right)
				Set<Symbol> last = new HashSet<>();
				last.addAll(((BinaryConcatenation) node).getLeft().getLast());
				last.addAll(((BinaryConcatenation) node).getRight().getLast());
				node.setLast(last);
			}
			else
			{
				// last(node) = last(right)
				node.setLast(((BinaryConcatenation) node).getRight().getLast());
			}
			return;
		}
		if (node instanceof Star)
		{
			// nullable(node) = true
			node.setNullable(true);
			// for each x in last(child) do
			for (Symbol x : ((Star) node).getOperand().getLast())
			{
				// follow(x) = follow(x) union first(child)
				Set<Symbol> follow = new HashSet<>();
				follow.addAll(x.getFollow());
				follow.addAll(((Star) node).getOperand().getFirst());
				x.setFollow(follow);
			}
			// first(node) = first(child)
			node.setFirst(((Star) node).getOperand().getFirst());
			// last(node) = last(child)
			node.setLast(((Star) node).getOperand().getLast());
			return;
		}
		throw new RegexException("Encountered a regex element of unknown type! " + node.getClass().getCanonicalName()); // this should never happen
	}
	
	/**
	 * Concatenates two NFAs.
	 * @param left - the first NFA
	 * @param right - the second NFA
	 * @return a NFA representing the concatenation of both given NFAs
	 */
	public static NFA NFAConcatenation(NFA left, NFA right)
	{
		// attributes of the new NFA
		Set<State> newStates = new HashSet<>();
		Set<String> newAlphabet = new HashSet<>();
		Set<Transition> newTransitionFunction = new HashSet<>();
		State newStartState;
		Set<State> newAcceptStates = new HashSet<>();
		
		// the new set of states equals the union of both sets
		newStates.addAll(left.getStates());
		newStates.addAll(right.getStates());
		
		// the new alphabet equals the union of both alphabets
		newAlphabet.addAll(left.getAlphabet());
		newAlphabet.addAll(right.getAlphabet());
		
		// the new transition function will equal the union of both old ones, plus one new epsilon transition
		// from each of the accept states of the first NFA to the start state of the second NFA
		newTransitionFunction.addAll(left.getTransitionFunction());
		newTransitionFunction.addAll(right.getTransitionFunction());
		for (State state : left.getAcceptStates())
		{
			newTransitionFunction.add(new Transition(state, Symbol.EPSILON, right.getStartState()));
		}
		
		// the new start state will be the start state of the first (left) NFA
		newStartState = left.getStartState();
		
		// the new set of accept states will be the accept states of the second (right) NFA
		newAcceptStates.addAll(right.getAcceptStates());
		
		return new NFA(newStates, newAlphabet, newTransitionFunction, newStartState, newAcceptStates);
	}
	
	/**
	 * Performs the star operation on a NFA.
	 * @param nfa - the NFA to perform the star operation on
	 * @return a NFA equivalent to the old NFA starred
	 */
	public static NFA NFAStar(NFA nfa)
	{
		// attributes of the new NFA
		Set<State> newStates = new HashSet<>();
		Set<String> newAlphabet = new HashSet<>();
		Set<Transition> newTransitionFunction = new HashSet<>();
		State newStartState;
		Set<State> newAcceptStates = new HashSet<>();
		
		// the new set of states equals the old one plus a new start state
		newStates.addAll(nfa.getStates());
		// the new start state will be a new state that has an epsilon transition to the old start states
		newStartState = new State();
		// and add it to the set of states
		newStates.add(newStartState);
		
		// the new alphabet equals the old alphabet
		newAlphabet.addAll(nfa.getAlphabet());
		
		// add the old transition function
		newTransitionFunction.addAll(nfa.getTransitionFunction());
		// for each old accept state, add a new transition from it to the old start state
		for (State state : nfa.getAcceptStates())
		{
			newTransitionFunction.add(new Transition(state, Symbol.EPSILON, nfa.getStartState()));
		}
		// and add a new epsilon transition from the new start state to the old start state
		newTransitionFunction.add(new Transition(newStartState, Symbol.EPSILON, nfa.getStartState()));
		
		// the new set of accept states will be the same as the old set plus the new start state
		newAcceptStates.addAll(nfa.getAcceptStates());
		newAcceptStates.add(newStartState);
		
		return new NFA(newStates, newAlphabet, newTransitionFunction, newStartState, newAcceptStates);
	}
	
	/**
	 * Generates the union of two NFAs.
	 * @param left - the first NFA
	 * @param right - the second NFA
	 * @return a NFA representing the union of both given NFAs
	 */
	public static NFA NFAUnion(NFA left, NFA right)
	{
		// attributes of the new NFA
		Set<State> newStates = new HashSet<>();
		Set<String> newAlphabet = new HashSet<>();
		Set<Transition> newTransitionFunction = new HashSet<>();
		State newStartState;
		Set<State> newAcceptStates = new HashSet<>();
		
		// the new set of states equals the union of both sets, plus one new state (the new start state)
		newStates.addAll(left.getStates());
		newStates.addAll(right.getStates());
		// the new start state will be a new state that has an epsilon transition to both of the old start states
		newStartState = new State();
		// and add it to the set of states
		newStates.add(newStartState);
		
		// the new alphabet equals the union of both alphabets
		newAlphabet.addAll(left.getAlphabet());
		newAlphabet.addAll(right.getAlphabet());
		
		// the new transition function will equal the union of both old ones, plus two new epsilon
		// transitions, from the new start state to both old start states
		newTransitionFunction.addAll(left.getTransitionFunction());
		newTransitionFunction.addAll(right.getTransitionFunction());
		newTransitionFunction.add(new Transition(newStartState, Symbol.EPSILON, left.getStartState()));
		newTransitionFunction.add(new Transition(newStartState, Symbol.EPSILON, right.getStartState()));
		
		// the new set of accept states will be the union of both old set of accept states
		newAcceptStates.addAll(left.getAcceptStates());
		newAcceptStates.addAll(right.getAcceptStates());
		
		return new NFA(newStates, newAlphabet, newTransitionFunction, newStartState, newAcceptStates);
	}
	
	public NFA minimize()
	{
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Converts this NFA into a Glushkov NFA, i.e. all epsilon transitions will be removed from this NFA.
	 * @return the Glushkov representation of this NFA
	 */
	public NFA convertToGlushkovAutomaton()
	{
		Set<Transition> newTransitionFunction = new HashSet<>();
		Set<State> newAcceptStates = new HashSet<>();
		
		// the set of new states is the set of the old states minus all states that have an incoming epsilon transition
		Set<State> glushkovStates = new HashSet<>();
		Set<State> statesReceivingEpsilonTransition = new HashSet<>();
		for (Transition transition : transitionFunction)
		{
			if (transition.getSymbol().equals(Symbol.EPSILON))
			{
				// if this transition is an epsilon transition, add its new state to the set of states receiving epsilon transitions
				statesReceivingEpsilonTransition.add(transition.getNewState());
			}
		}
		// then compute the difference
		glushkovStates.addAll(Utility.symmetricDifference(states, statesReceivingEpsilonTransition));
		
		// now merge the epsilon transitions into non-epsilon transitions and compute the new accept states
		for (State glushkovState : glushkovStates)
		{
			boolean epsilonClosureContainsAcceptState = false;
			for (State state : findReachableStates(glushkovState, Symbol.EPSILON))
			{
				if (Utility.contains(acceptStates, state))
				{
					epsilonClosureContainsAcceptState = true;
				}
				// repair the transition function
				for (Transition transition : transitionFunction)
				{
					// if this state in the epsilon closure of the original Glushkov state has a transition that is not an
					// epsilon transition, add a new transition to the transition function with the origin state being
					// the Glushkov state and the symbol and new state staying the same
					if (transition.getOriginState().equals(state) && !transition.getSymbol().equals(Symbol.EPSILON))
					{
						newTransitionFunction.add(new Transition(glushkovState, transition.getSymbol(), transition.getNewState()));
					}
				}
			}
			// this state is an accept state if its epsilon closure contains an accept state or this state itself was part of
			// the original accept states
			if (epsilonClosureContainsAcceptState || Utility.contains(acceptStates, glushkovState))
			{
				newAcceptStates.add(glushkovState);
			}
		}
		// and finally add the non-epsilon transitions between states that are not epsilon receiving states
		for (Transition transition : transitionFunction)
		{
			if (Utility.contains(glushkovStates, transition.getOriginState()) && Utility.contains(glushkovStates, transition.getNewState()))
			{
				newTransitionFunction.add(transition);
			}
		}
		
		return new NFA(glushkovStates, alphabet, newTransitionFunction, startState, newAcceptStates);
	}
	
	/**
	 * Finds all reachable states from the origin state when the given symbol is processed.
	 * Also follows epsilon transitions.
	 * @param originStats - the state to start from
	 * @param symbol - the symbol to transition through
	 * @return the set of reachable states from the given origin state and symbol
	 */
	private Set<State> findReachableStates(State originState, String symbol)
	{
		Set<State> originStates = new HashSet<>();
		originStates.add(originState);
		return findReachableStates(originStates, symbol, new HashSet<>());
	}

	/**
	 * Finds all reachable states from the set of origin states when the given symbol is processed.
	 * Also follows epsilon transitions.
	 * @param originStates - the set of states to start from
	 * @param symbol - the symbol to transition through
	 * @param visitedStates - a set of states already visited
	 * @return the set of reachable states from the given origin states and symbol
	 */
	private Set<State> findReachableStates(Set<State> originStates, String symbol, Set<State> visitedStates)
	{
		Set<State> reachableStates = new HashSet<>();
		Set<Transition> transitionsFromThisState = new HashSet<>();
		// find the transitions for each origin state
		for (State state : originStates)
		{
			if (!visitedStates.contains(state))
			{
				// only add this state to the visited states if we are in the epsilon checking stage
				if (symbol.equals(Symbol.EPSILON))
				{
					visitedStates.add(state);
				}
				transitionsFromThisState.addAll(findTransitions(state, symbol));
			}
		}
		// and add the target states to the list of reachable states
		for (Transition transition : transitionsFromThisState)
		{
			reachableStates.add(transition.getNewState());
		}
		// if there were reachable states, check for epsilon transitions
		if (!transitionsFromThisState.isEmpty())
		{
			reachableStates.addAll(findReachableStates(reachableStates, Symbol.EPSILON, visitedStates));
		}
		
		return reachableStates;
	}
	
	/**
	 * Finds all transitions with the given state as origin state and symbol.
	 * @param state - the origin state
	 * @param symbol - the symbol
	 * @return all transitions with the given origin state and symbol
	 */
	private Set<Transition> findTransitions(State state, String symbol)
	{
		Set<Transition> result = new HashSet<>();
		
		for (Transition transition : transitionFunction)
		{
			if (transition.getOriginState().equals(state) && transition.getSymbol().equals(symbol))
			{
				result.add(transition);
			}
		}
		
		return result;
	}
	
	/**
	 * Finds a state with the specified name in the set of given states.
	 * @param states - the set of states to search for a state with the specified name in
	 * @param name - the name of the state to search for
	 * @return a state with the specified name, null if no such state was found
	 */
	private State findStateWithName(Set<State> states, String name) throws StateException
	{
		for (State state : states)
		{
			if (state.getName().equals(name))
			{
				return state;
			}
		}
		
		throw new StateException("Found no state with name: " + name + "!");
	}
	
	/**
	 * Consolidates the names of all states in the given set.
	 * @param states - the set of states to generate a name from
	 * @return a state name consisting of all the names from the states from the given set
	 */
	private String consolidateStateName(Set<State> states)
	{
		String stateName = "{";
		State[] stateArray = states.toArray(new State[states.size()]); //new State[states.size()];
		Arrays.sort(stateArray);
		for (State state : stateArray)
		{
			stateName += state.getName() + ",";
		}
		if (stateName.length() > 1)
		{
			stateName = stateName.substring(0, stateName.length()-1); // delete the last comma
		}
		else
		{
			stateName += "empty";
		}
		stateName += "}";
		
		return stateName;
	}
	
	/**
	 * Determines whether a given state is present in a set.
	 * @param states - the set of states to check
	 * @param state - the state to check for
	 * @return true if one of the states in the set has the same name as the specified state, false otherwise
	 */
	private boolean setContainsState(Set<Set<State>> states, Set<State> state)
	{
		for (Set<State> s : states)
		{
//			if (consolidateStateName(s).equals(consolidateStateName(state)))
//			{
//				return true;
//			}
			if (s.equals(state))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Converts this NFA to an equivalent DFA.
	 * @return a DFA equivalent to this NFA
	 * @throws DuplicateTransitionException when the new transition function contains a duplicate transition
	 * @throws StateException when the new start state cannot be found among the set of new states
	 */
	public DFA convertToDFA() throws DuplicateTransitionException, StateException
	{		
		// remove the epsilon symbol from the alphabet (if present)
		Set<String> newAlphabet = new HashSet<>(alphabet);
		newAlphabet.remove(Symbol.EPSILON);
		
		// compute the new transition function AND the new set of states at the same time
		// first we add all the original states to the set of new states
		Set<Set<State>> tempNewStates = new HashSet<>();
		Set<Transition> newTransitionFunction = new HashSet<>();
		Set<Set<State>> iterableNewStates = new HashSet<>();
		for (State state : states)
		{
			Set<State> tempNewState = new HashSet<>();
			tempNewState.add(state);
			iterableNewStates.add(tempNewState);
			tempNewStates.add(tempNewState);
		}
		// add the new start state to the list of base states
		Set<State> reachableStatesFromStartState = findReachableStates(startState, Symbol.EPSILON);
		// add the original start state itself
		// first check whether the original state is already present (because of epsilon transitions for example)
		boolean foundStartState = false;
		for (State state : reachableStatesFromStartState)
		{
			if (state.equals(startState))
			{
				foundStartState = true;
			}
		}
		if (!foundStartState)
		{
			reachableStatesFromStartState.add(findStateWithName(states, startState.getName()));
		}
		iterableNewStates.add(reachableStatesFromStartState);
		tempNewStates.add(reachableStatesFromStartState);
		Log.debug("Converting NFA to DFA:\n[PRE-PASS]\ntempNewStates contains:");
		for (Set<State> state : tempNewStates)
		{
			Log.debug(consolidateStateName(state));
		}
		// then we compute the new transitions from these states and add the resulting target states to the set of new states
		// and repeat this until no new states are added
		Set<Set<State>> computedNewStates = new HashSet<>();
		int pass = 1;
		Long dfaStatesComputationStartTime = System.currentTimeMillis();
		do
		{
			computedNewStates.clear();
			//Log.debug("[PASS " + pass++ + "]");
			int passProgress = 0;
			int passTotal = iterableNewStates.size();
			long dfaStatesComputationPassStartTime = System.currentTimeMillis();
			long startTimeOfComputingBlockOf50States = System.currentTimeMillis();
			for (Set<State> originState : iterableNewStates)
			{
				//Log.debug("Computing transitions from: " + consolidateStateName(originState));
				for (String symbol : newAlphabet)
				{
					Set<State> targetState = findReachableStates(originState, symbol, new HashSet<>());
					Transition newTransition = new Transition(new State(consolidateStateName(originState)), symbol, new State(consolidateStateName(targetState)));
					newTransitionFunction.add(newTransition);
					//String foundTransition = "\tfound: " + newTransition.toString();
					if (!setContainsState(tempNewStates, targetState))
					{
						//foundTransition += " - new state";
						computedNewStates.add(targetState);
						tempNewStates.add(targetState);
					}
					//Log.debug(foundTransition);
				}
				System.out.println("\tPass " + pass + ": " + ++passProgress + "/" + passTotal);
				// display an estimate of the remaining time to compute all states in this pass
				if (passProgress % 50 == 0)
				{
					long timeComputingPrevious50States = System.currentTimeMillis() - startTimeOfComputingBlockOf50States;
					startTimeOfComputingBlockOf50States = System.currentTimeMillis();
					int numberOfSTatesStillToCompute = passTotal - passProgress;
					System.out.println("\tEstimated time left for this pass: " + ((numberOfSTatesStillToCompute / 50 * timeComputingPrevious50States) / 1000) + " seconds.");
				}
			}
			iterableNewStates.clear();
			iterableNewStates.addAll(computedNewStates);
//			if (!computedNewStates.isEmpty())
//			{
//				String foundNewStates = " -- New states this pass: ";
//				for (Set<State> state : computedNewStates)
//				{
//					foundNewStates += consolidateStateName(state) + " ";
//				}
//				System.out.println(foundNewStates);
//			}
//			else
//			{
//				System.out.println(" -- Found no new states.");
//			}
			passProgress = 0;
			pass++;
			long totalTimeMinutes = (System.currentTimeMillis() - dfaStatesComputationPassStartTime) / 1000 / 60;
			long totalTimeSecondsRest = totalTimeMinutes % 60;
			System.out.println("\tThis pass took " + totalTimeMinutes + " minutes and " + totalTimeSecondsRest + " seconds.");
		}
		while (!computedNewStates.isEmpty());
		long totalTimeMinutes = (System.currentTimeMillis() - dfaStatesComputationStartTime) / 1000 / 60;
		long totalTimeSecondsRest = totalTimeMinutes % 60;
		System.out.println("\tComputing states of DFA took " + totalTimeMinutes + " minutes and " + totalTimeSecondsRest + " seconds.");
		
		// now convert the set containing 'states' (which are actually sets of states) into a valid set of new states
		Set<State> newStates = new HashSet<>();
		for (Set<State> state : tempNewStates)
		{
			State s = new State(consolidateStateName(state));
			newStates.add(s);
		}
		
		// find the new start state among the new set of states for the DFA
		String consolidatedStartStateName = consolidateStateName(reachableStatesFromStartState);
		State newStartState = findStateWithName(newStates, consolidatedStartStateName);
		
		// find the new accept states by checking which new states contain at least one of the original accept states
		Set<State> newAcceptStates = new HashSet<>();	
		for (Set<State> state : tempNewStates)
		{
			for (State stateElement : state)
			{
				for (State s : acceptStates)
				{
					if (stateElement.equals(s))
					{
						for (State consolidatedState : newStates)
						{
							if (consolidateStateName(state).equals(consolidatedState.getName()))
							{
								newAcceptStates.add(consolidatedState);
							}
						}
					}
				}
			}
		}
		
		return new DFA(newStates, newAlphabet, newTransitionFunction, newStartState, newAcceptStates);
	}
	
	/**
	 * Returns the number of transitions in this automaton.
	 */
	public int transitionCount()
	{
		return transitionFunction.size();
	}
	
	/**
	 * Calculates per number of incoming transitions the number of states that have this amount of incoming transitions.
	 * @return an array whose indices are the number of incoming transitions and values the number of states 
	 * with this amount of incoming transitions
	 */
	private int[] statesPerIncomingTransitionCount()
	{
		int[] stateCount = new int[transitionFunction.size() + 1]; // max size of the array will always be the total number of transitions
		if (stateCount.length <= 1)
		{
			return stateCount;
		}
		
		int incomingTransitionCount = 0;
		// for every state
		for (State state : states)
		{
			// check every transition
			for (Transition transition : transitionFunction)
			{
				// and if that transition's new state matches the current iteration's state
				if (transition.getNewState().equals(state))
				{
					// up the incoming transition count
					incomingTransitionCount++;
				}
			}
			stateCount[incomingTransitionCount]++;
			incomingTransitionCount = 0;
		}
		
		return stateCount;
	}
	
	/**
	 * Prints statistics on the transitions of this NFA. Includes the total number of transitions and per number of incoming
	 * transitions the amount of states that have this number of incoming transitions.
	 */
	public void printTransitionReport()
	{
		int[] stateCount = statesPerIncomingTransitionCount();
		
		//Log.info("Total amount of transitions: " + transitionFunction.size());
		for (int i=0; i<stateCount.length; i++)
		{
			if (stateCount[i] != 0)
			{
				Log.info("\tThere are " + stateCount[i] + " states with " + i + " incoming transitions.");
			}
		}
	}
	
	/**
	 * Returns a string containing the formal description of this NFA.
	 */
	public String toString()
	{
		String result = "[NFA]\n";
		
		result += "States:\n";
		for (State state : states)
		{
			result += state.getName() + "\n";
		}
		result += "Alphabet:\n";
		for (String symbol : alphabet)
		{
			if (symbol.equals(Symbol.EPSILON))
			{
				result += symbol + "\n";
			}
			else
			{
				result += symbol + "\n";
			}
		}
		result += "Transition function:\n";
		for (Transition transition : transitionFunction)
		{
			result += transition.toString() + "\n";
			System.out.println(transition.toString());
		}
		result += "Start state:\n" + startState.getName() + "\n";
		result += "Accept states:\n";
		for (State state : acceptStates)
		{
			result += state.getName() + "\n";
		}
		result = result.substring(0, result.length()-1);
		
		return result;
	}
	
	public Set<State> getStates()
	{
		return states;
	}
	
	public Set<String> getAlphabet()
	{
		return alphabet;
	}
	
	public Set<Transition> getTransitionFunction()
	{
		return transitionFunction;
	}
	
	public State getStartState()
	{
		return startState;
	}
	
	public Set<State> getAcceptStates()
	{
		return acceptStates;
	}
}
