package automata;

import regex.RegexElement;

public class GNFATransition
{
	private State originState;
	private RegexElement rootOfRegexLabel;
	private State newState;
	
	GNFATransition(State originState, RegexElement rootOfRegexLabel, State newState)
	{
		this.originState = originState;
		this.rootOfRegexLabel = rootOfRegexLabel;
		this.newState = newState;
	}
	
	public State getOriginState()
	{
		return originState;
	}
	
	public State getNewState()
	{
		return newState;
	}
	
	public RegexElement getRootOfRegexLabel()
	{
		return rootOfRegexLabel;
	}
	
	/**
	 * Returns a string of the form (state,regex) = state. Example:
	 * (1,(a|b)*) = 3
	 */
	public String toString()
	{
		return "(" + originState.getName() + "," + rootOfRegexLabel.toString() + ") = " + newState.getName();
	}
}
