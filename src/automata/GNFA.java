package automata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import exceptions.IllFormedAutomatonException;
import exceptions.RegexException;
import main.Log;
import regex.Concatenation;
import regex.RegexElement;
import regex.Star;
import regex.Symbol;
import regex.Union;

public class GNFA
{
	private Set<State> states = new HashSet<>();
	private Set<String> alphabet = new HashSet<>();
	private Set<GNFATransition> transitionFunction = new HashSet<>();
	private State startState;
	private State acceptStates;
	
	/**
	 * Constructs a GNFA from the given parameters. Accepts sets.
	 * @param states - the set of states
	 * @param alphabet - the set of symbols (strings) forming the alphabet
	 * @param transitionFunction - the set of transitions
	 * @param startState - the start state
	 * @param acceptStates - the set of accept states
	 * @return a GNFA with the given parameters
	 */
	public GNFA(Set<State> states, Set<String> alphabet, Set<GNFATransition> transitionFunction, State startState, State acceptStates)
	{
		this.states = states;
		this.alphabet = alphabet;
		this.transitionFunction = transitionFunction;
		this.startState = startState;
		this.acceptStates = acceptStates;
	}
	
	public GNFA(DFA dfa)
	{
		alphabet = dfa.getAlphabet();
		alphabet.add(Symbol.EPSILON);
		// add new start and accept states plus all old states
		states.addAll(dfa.getStates());
		
		// make sure we do not add start and accept states with names already present in the set of states
		boolean foundUnusedName = true;
		String startStateName = "start";
		do
		{
			foundUnusedName = true;
			for (State state : states)
			{
				if (state.getName().equals(startStateName))
				{
					foundUnusedName = false;
					startStateName += "_";
					break;
				}
			}
		}
		while (!foundUnusedName);
		startState = new State(startStateName);
		foundUnusedName = true;
		String acceptStateName = "accept";
		do
		{
			foundUnusedName = true;
			for (State state : states)
			{
				if (state.getName().equals(acceptStateName))
				{
					foundUnusedName = false;
					acceptStateName += "_";
					break;
				}
			}
		}
		while (!foundUnusedName);
		acceptStates = new State(acceptStateName);
		
		states.add(startState);
		states.add(acceptStates);
		// new epsilon transition from new start state to old
		transitionFunction.add(new GNFATransition(startState, new Symbol(Symbol.EPSILON), dfa.getStartState()));
		// new epsilon transitions from old accept states to new
		for (State oldAcceptState : dfa.getAcceptStates())
		{
			transitionFunction.add(new GNFATransition(oldAcceptState, new Symbol(Symbol.EPSILON), acceptStates));
		}
		// consolidate transitions from the same origin to the same new state into a single transition
		// first construct a list containing sets of transitions with the same direction
		Set<Set<Transition>> sameDirectionTransitionsSet = new HashSet<>();
		for (Transition outer : dfa.getTransitionFunction())
		{
			Set<Transition> sameDirectionTransitions = new HashSet<>();
			sameDirectionTransitions.add(outer);
			for (Transition inner : dfa.getTransitionFunction())
			{
				if (outer.getOriginState().equals(inner.getOriginState()) &&
						outer.getNewState().equals(inner.getNewState()) &&
						!(outer.getSymbol().equals(inner.getSymbol()))
						)
				{
					sameDirectionTransitions.add(inner);
				}
			}
			sameDirectionTransitionsSet.add(sameDirectionTransitions);
		}
		// then consolidate each transition in a given same direction set into one single transition whose label is the union of each of its composite transitions' labels
		for (Set<Transition> sameDirectionTransitions : sameDirectionTransitionsSet)
		{
			RegexElement root = null;
			boolean isFirst = true;
			Symbol firstSymbol = null;
			for (Transition transition : sameDirectionTransitions)
			{
				if (isFirst)
				{
					firstSymbol = new Symbol(transition.getSymbol());
					isFirst = false;
				}
				else
				{
					if (firstSymbol != null)
					{
						root = new Union(firstSymbol, new Symbol(transition.getSymbol()));
						firstSymbol = null;
					}
					else
					{
						root = new Union(root, new Symbol(transition.getSymbol()));
					}
				}
			}
			if (root == null)
			{
				root = firstSymbol;
			}
			// construct the new transition and add it to the transition function
			transitionFunction.add(new GNFATransition(
					sameDirectionTransitions.iterator().next().getOriginState(),
					root,
					sameDirectionTransitions.iterator().next().getNewState()));
		}
		
		// now add empty language transitions between states that do not have transitions between them yet
		// first check transitions from the start state to every other state
		Set<State> statesWithoutStart = new HashSet<>(states);
		statesWithoutStart.remove(startState);
		for (State state : statesWithoutStart)
		{
			boolean foundTransition = false;
			for (GNFATransition transition : transitionFunction)
			{
				if (transition.getOriginState().equals(startState) && transition.getNewState().equals(state))
				{
					foundTransition = true;
					break;
				}
			}
			if (!foundTransition)
			{
				transitionFunction.add(new GNFATransition(startState, new Symbol(Symbol.EMPTY_LANGUAGE), state));
			}
		}
		// then check transitions from every other state to the accept state
		Set<State> statesWithoutAccept = new HashSet<>(states);
		statesWithoutAccept.remove(acceptStates);
		for (State state : statesWithoutAccept)
		{
			boolean foundTransition = false;
			for (GNFATransition transition : transitionFunction)
			{
				if (transition.getOriginState().equals(state) && transition.getNewState().equals(acceptStates))
				{
					foundTransition = true;
					break;
				}
			}
			if (!foundTransition)
			{
				transitionFunction.add(new GNFATransition(state, new Symbol(Symbol.EMPTY_LANGUAGE), acceptStates));
			}
		}
		// and lastly check transitions between all states (minus the start and accept state)
		Set<State> statesWithoutAcceptAndStart = new HashSet<>(states);
		statesWithoutAcceptAndStart.remove(startState);
		statesWithoutAcceptAndStart.remove(acceptStates);
		for (State outer : statesWithoutAcceptAndStart)
		{
			for (State inner : statesWithoutAcceptAndStart)
			{
				boolean foundTransition = false;
				for (GNFATransition transition : transitionFunction)
				{
					if (transition.getOriginState().equals(outer) && transition.getNewState().equals(inner))
					{
						foundTransition = true;
						break;
					}
				}
				if (!foundTransition)
				{
					transitionFunction.add(new GNFATransition(outer, new Symbol(Symbol.EMPTY_LANGUAGE), inner));
				}
			}
		}
	}
	
	/**
	 * Converts this GNFA into a regex.
	 * @return the root of the regex equivalent to this GNFA
	 * @throws IllFormedAutomatonException if this GNFA is ill-formed
	 */
	public RegexElement convertToRegex() throws IllFormedAutomatonException, RegexException
	{
		return convertToRegex(this);
	}
	
	/**
	 * Private helper method called by the public convertToRegex() to aid recursion.
	 * @param gnfa - the GNFA to convert to a regex
	 * @return a regex element containing the root of the AST representing the regex representing the given GNFA
	 * @throws IllFormedAutomatonException if the given GNFA is ill-formed
	 */
	private RegexElement convertToRegex(GNFA gnfa) throws IllFormedAutomatonException, RegexException
	{
		// if the amount of states in the GNFA equals two we cannot reduce it any further, so we return the label of its only transition
		if (gnfa.getStates().size() == 2)
		{
			Iterator<GNFATransition> it = gnfa.getTransitionFunction().iterator();
			if (it.hasNext())
			{
				return it.next().getRootOfRegexLabel();
			}
			else
			{
				throw new IllFormedAutomatonException("A GNFA with two states should not have no transitions!");
			}
		}
		// check whether the GNFA is a valid GNFA
		//TODO change the place of this check to the GNFA itself?
		if (gnfa.getStates().size() <= 1)
		{
			throw new IllFormedAutomatonException("A GNFA cannot have less than two states!");
		}
		
		// set up some useful sets and select the state to rip
		Set<State> oldStatesWithoutStartAndAccept = new HashSet<>(gnfa.getStates());
		oldStatesWithoutStartAndAccept.remove(gnfa.getStartState());
		oldStatesWithoutStartAndAccept.remove(gnfa.getAcceptState());
		
		State rip = oldStatesWithoutStartAndAccept.iterator().next();
		for (State state : oldStatesWithoutStartAndAccept)
		{
			if (state.getName().compareToIgnoreCase(rip.getName()) < 0)
			{
				rip = state;
			}
		}
		
		Set<State> newStates = new HashSet<>(gnfa.getStates());
		newStates.remove(rip);
		Set<State> newStatesWithoutStart = new HashSet<>(newStates);
		newStatesWithoutStart.remove(gnfa.getStartState());
		Set<State> newStatesWithoutAccept = new HashSet<>(newStates);
		newStatesWithoutAccept.remove(gnfa.getAcceptState());
		
		// repair the transition function
		Set<GNFATransition> newTransitionFunction = new HashSet<>();
		for (State i : newStatesWithoutAccept)
		{
			for (State j : newStatesWithoutStart)
			{
				RegexElement R1 = null;
				RegexElement R2 = null;
				RegexElement R3 = null;
				RegexElement R4 = null;
				
				for (GNFATransition transition : gnfa.getTransitionFunction())
				{
					// R1 = (i, rip)
					if (transition.getOriginState().equals(i) && transition.getNewState().equals(rip))
					{
						R1 = transition.getRootOfRegexLabel();
					}
					// R2 = (rip, rip)
					if (transition.getOriginState().equals(rip) && transition.getNewState().equals(rip))
					{
						R2 = transition.getRootOfRegexLabel();
					}
					// R3 = (rip, j)
					if (transition.getOriginState().equals(rip) && transition.getNewState().equals(j))
					{
						R3 = transition.getRootOfRegexLabel();
					}
					// R4 = (i, j)
					if (transition.getOriginState().equals(i) && transition.getNewState().equals(j))
					{
						R4 = transition.getRootOfRegexLabel();
					}
				}
				Log.info(
						"[ rip=" + rip.getName() + " i=" + i.getName() + " j=" + j.getName() + " ]:" +
						"[ R1=" + R1.toString() + " R2=" + R2.toString() + " R3=" + R3.toString() + " R4=" + R4.toString() + " ]:" +
						"[ new label=" + generateGNFATransitionLabel(R1, R2, R3, R4).toString() + " ]"
						);
				newTransitionFunction.add(new GNFATransition(i, generateGNFATransitionLabel(R1, R2, R3, R4), j));
			}
		}
		
		return convertToRegex(new GNFA(newStates, gnfa.getAlphabet(), newTransitionFunction, gnfa.getStartState(), gnfa.getAcceptState()));
	}
	
	/**
	 * Private helper method that generates a new regex label of the form (R1+(R2)*+R3)|R4 where + represents concatenation.
	 * @param R1 - the element representing the transition from state i to state rip
	 * @param R2 - the element representing the transition from state rip to state rip
	 * @param R3 - the element representing the transition from state rip to state j
	 * @param R4 - the element representing the transition from state i to state j
	 * @return the root of the regex element representing the new label
	 * @throws RegexException when the list of concatenation elements contains less than two elements
	 */
	private RegexElement generateGNFATransitionLabel(RegexElement R1, RegexElement R2, RegexElement R3, RegexElement R4) throws RegexException
	{
		// if any of R1 or R3 are the empty language, the entire concatenation will be the empty language and we will get:
		// (empty_language | R4) which is equal to R4 (in this case we simply return R4 which is the original label from state i to state j)
		if ((R1 instanceof Symbol && ((Symbol) R1).isEmptyLanguage()) ||
				/*(R2 instanceof Symbol && ((Symbol) R2).isEmptyLanguage()) ||*/
				(R3 instanceof Symbol && ((Symbol) R3).isEmptyLanguage()))
		{
			return R4;
		}
		else
		{
			List<RegexElement> concatElements = new ArrayList<>();
			// for R1, R2 and R3 check if they are the epsilon symbol, if not, concatenate them
			if (!(R1 instanceof Symbol && ((Symbol) R1).isEpsilon()))
			{
				concatElements.add(R1);
			}
			// do not concatenate R2 if it is the empty language
			if ( !(R2 instanceof Symbol && ((Symbol) R2).isEpsilon()) && !(R2 instanceof Symbol && ((Symbol) R2).isEmptyLanguage()))
			{
				concatElements.add(new Star(R2));
			}
			if (!(R3 instanceof Symbol && ((Symbol) R3).isEpsilon()))
			{
				concatElements.add(R3);
			}
			RegexElement concat;
			if (concatElements.size() == 1)
			{
				concat = concatElements.get(0);
			}
			else
			{
				// if the size of the concatenation's element list is zero, we have encountered R1 = epsilon and R3 = epsilon
				// in this case the new transition's label is epsilon
				if (concatElements.size() == 0)
				{
					concat = new Symbol(Symbol.EPSILON);
				}
				else
				{
					concat = new Concatenation(concatElements);
				}
			}
			// return (concatenation | R4) if R4 is not the empty language, otherwise just return the concatenation
			if (!(R4 instanceof Symbol && ((Symbol) R4).isEmptyLanguage()))
			{
				return new Union(concat, R4);
			}
			else
			{
				return concat;
			}
		}
	}
	
	/**
	 * Returns a string containing the formal description of this GNFA.
	 */
	public String toString()
	{
		String result = "[GNFA]\n";
		
		result += "States:\n";
		for (State state : states)
		{
			result += state.getName() + "\n";
		}
		result += "Alphabet:\n";
		for (String symbol : alphabet)
		{
			result += symbol + "\n";
		}
		result += "Transition function:\n";
		for (GNFATransition transition : transitionFunction)
		{
			result += transition.toString() + "\n";
		}
		result += "Start state:\n" + startState.getName() + "\n";
		result += "Accept states:\n";
		result += acceptStates.getName() + "\n";
		result = result.substring(0, result.length()-1);
		
		return result;
	}
	
	public Set<State> getStates()
	{
		return states;
	}
	
	public Set<String> getAlphabet()
	{
		return alphabet;
	}
	
	public Set<GNFATransition> getTransitionFunction()
	{
		return transitionFunction;
	}
	
	public State getStartState()
	{
		return startState;
	}
	
	public State getAcceptState()
	{
		return acceptStates;
	}
}
