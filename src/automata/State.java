package automata;

import main.Utility;

public class State implements Comparable<State>
{
	public static final String EMPTY_STATE = "EMPTY_STATE";
	private String name;
	
	public State()
	{
		name = String.valueOf(Utility.getNewStateName());
	}
	
	public State(String name)
	{
		this.name = name;
	}
	
	public State(int name)
	{
		this.name = String.valueOf(name);
	}
	
	public void setName(int name)
	{
		this.name = String.valueOf(name);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String toString()
	{
		return getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Returns true only when the object being compared to is of type {@code State} and the name of that state is equal
	 * to the name of this state.
	 * @param obj - the state to compare to
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		State other = (State) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(State o)
	{
		return this.name.compareTo(o.name);
		//return 0;
	}
	
	
//	public boolean equals(Object o)
//	{
//		if (!(o instanceof State))
//		{
//			return false;
//		}
//		
//		return name.equals(((State) o).getName());
//	}
}
