package automata;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import exceptions.DuplicateTransitionException;
import exceptions.IllFormedAutomatonException;
import exceptions.RegexException;
import exceptions.StateException;
import main.Log;
import main.Utility;
import main.Utility.AUTOMATON_READER_STATE;
import regex.RegexElement;

public class DFA
{
	private Set<State> states;
	private Set<String> alphabet;
	private Set<Transition> transitionFunction;
	private State startState;
	private Set<State> acceptStates;
	
	/**
	 * Constructs a DFA from the given parameters. Accepts sets. Automatically removes unreachable states should they be present in the parameters.
	 * @param states - the set of states
	 * @param alphabet - the set of symbols (strings) forming the alphabet
	 * @param transitionFunction - the set of transitions
	 * @param startState - the start state
	 * @param acceptStates - the set of accept states
	 * @return a DFA with the given parameters
	 * @throws DuplicateTransitionException when the transition function contains a duplicate transition (two transitions are duplicate when they have the same origin state and symbol)
	 */
	public DFA(Set<State> states, Set<String> alphabet, Set<Transition> transitionFunction, State startState, Set<State> acceptStates) throws DuplicateTransitionException
	{
		this.states = states;
		this.alphabet = alphabet;
		
		Transition[] transitions = transitionFunction.toArray(new Transition[transitionFunction.size()]);
		for (int i=0; i<transitions.length-1; i++)
		{
			for (int j=i+1; j<transitions.length; j++)
			{
				if (transitions[i].isDuplicateTransition(transitions[j]))
				{
					throw new DuplicateTransitionException("Cannot construct DFA: there are two transitions with the same origin state and symbol!");
				}
			}
		}
		this.transitionFunction = transitionFunction;
		this.startState = startState;
		this.acceptStates = acceptStates;
		
		// guarantee that this DFA will not have unreachable states
		removeUnreachableStates(this.states, this.transitionFunction, this.startState, this.acceptStates);
	}
	
	/**
	 * Constructs a DFA from the given parameters. Accepts arrays.
	 * @param states - the set of states
	 * @param alphabet - the set of symbols (strings) forming the alphabet
	 * @param transitionFunction - the set of transitions
	 * @param startState - the start state
	 * @param acceptStates - the set of accept states
	 * @return a DFA with the given parameters
	 * @throws DuplicateTransitionException when the transition function contains a duplicate transition (two transitions are duplicate when they have the same origin state and symbol)
	 */
	public static DFA createDFA(State[] states, String[] alphabet, Transition[] transitionFunction, State startState, State[] acceptStates) throws DuplicateTransitionException
	{
		Set<State> newStates = new HashSet<>(states.length);
		for (State state : states)
		{
			newStates.add(state);
		}
		Set<String> newAlphabet = new HashSet<>(alphabet.length);
		for (String symbol : alphabet)
		{
			newAlphabet.add(symbol);
		}
		Set<Transition> newTransitionFunction = new HashSet<>(transitionFunction.length);
		for (Transition transition : transitionFunction)
		{
			newTransitionFunction.add(transition);
		}
		// start state can be directly copied from the parameters of this method
		Set<State> newAcceptStates = new HashSet<>(acceptStates.length);
		for (State state : acceptStates)
		{
			newAcceptStates.add(state);
		}
		return new DFA(newStates, newAlphabet, newTransitionFunction, startState, newAcceptStates);
	}
	
	/**
	 * Constructs a DFA from a file.
	 * @param path - the path to the file
	 * @return a DFA
	 * @throws IOException an IOException is thrown whenever there is an error in reading the DFA from the file
	 * @throws DuplicateTransitionException is thrown whenever the transition function contains a duplicate transition
	 */
	public static DFA createDFA(String path) throws IOException, DuplicateTransitionException
	{
		Set<State> states = new HashSet<>();
		Set<String> alphabet = new HashSet<>();
		Set<Transition> transitionFunction = new HashSet<>();
		State startState = null;
		Set<State> acceptStates = new HashSet<>();	
		
		try (BufferedReader reader = new BufferedReader(new FileReader(path)))
		{
			String line;
			AUTOMATON_READER_STATE readerState = AUTOMATON_READER_STATE.INIT;
			while ((line = reader.readLine()) != null)
			{
				// if the line is a comment go to the next line
				if (line.startsWith("#"))
				{
					continue;
				}
				switch (line)
				{
					case "[States]":
						if (readerState == AUTOMATON_READER_STATE.INIT)
						{
							readerState = AUTOMATON_READER_STATE.STATES;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Alphabet]":
						if (readerState == AUTOMATON_READER_STATE.STATES)
						{
							readerState = AUTOMATON_READER_STATE.ALPHABET;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Transition Function]":
						if (readerState == AUTOMATON_READER_STATE.ALPHABET)
						{
							readerState = AUTOMATON_READER_STATE.TRANSITION_FUNCTION;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Start State]":
						if (readerState == AUTOMATON_READER_STATE.TRANSITION_FUNCTION)
						{
							readerState = AUTOMATON_READER_STATE.START_STATE;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					case "[Accept States]":
						if (readerState == AUTOMATON_READER_STATE.START_STATE)
						{
							readerState = AUTOMATON_READER_STATE.ACCEPT_STATES;
							break;
						}
						else
						{
							throw new IOException("Error reading file: labels not in correct order. Use the order: [States], [Alphabet], [Transition Function], [Start State], [Accept States].");
						}
						
					default:
						// continue with the next line if this one is empty
						if (line.isEmpty())
						{
							break;
						}
						switch (readerState)
						{
							case INIT:
								throw new IOException("Error reading file: current line has no label!");
								
							case STATES:
								states.add(new State(line));
								break;
								
							case ALPHABET:
								alphabet.add(line);
								break;
								
							case TRANSITION_FUNCTION:
								// remove all unnecessary characters from the line
								line = line.replaceAll("[()]", "");
								// split the line into its components, index 0 is the origin state, index 1 is the symbol and index 2 is the new state
								String[] parameters = line.split("[=,]");
								// if the length of the parameter array is not equal to 3 something has gone wrong
								if (parameters.length != 3)
								{
									throw new IOException("Error reading file: transition has not been defined correctly! Use the format: (state,symbol)=state");
								}
								// find the state objects belonging to this transition
								State originState = null;
								State newState = null;
								for (State state : states)
								{
									// if the state is equal to the origin state
									if (state.getName().equals(parameters[0]))
									{
										originState = state;
									}
									// if the state is equal to the new state
									if (state.getName().equals(parameters[2]))
									{
										newState = state;
									}
								}
								// if one of the states speficied in the transition was not found in the set of states, throw an exception
								if (originState == null || newState == null)
								{
									throw new IOException("Error reading file: a state in a transition does not match any state in the state set!");
								}
								transitionFunction.add(new Transition(originState, parameters[1], newState));
								break;
							
							case START_STATE:
								State start = null;
								for (State state : states)
								{
									if (state.getName().equals(line))
									{
										start = state;
										break;
									}
								}
								if (start == null)
								{
									throw new IOException("Error reading file: the start state did not match any state in the state set!");
								}
								startState = start;
								break;
							
							case ACCEPT_STATES:
								State acceptState = null;
								for (State state : states)
								{
									if (state.getName().equals(line))
									{
										acceptState = state;
									}
								}
								if (acceptState == null)
								{
									throw new IOException("Error reading file: one of the accept states did not match any state in the state set!");
								}
								acceptStates.add(acceptState);
								break;
						}
						break;
				}
			}
		}
		
		return new DFA(states, alphabet, transitionFunction, startState, acceptStates);
	}
	
	/**
	 * Converts this DFA to an equivalent regex.
	 * @return the root of the regex AST
	 */
	public RegexElement convertToRegex() throws IllFormedAutomatonException, RegexException
	{
		return new GNFA(this).convertToRegex();
	}
	
	/**
	 * Converts this DFA to an equivalent GNFA.
	 * @return an equivalent GNFA
	 */
	public GNFA convertToGNFA()
	{
		return new GNFA(this);
	}
	
	/**
	 * Minimizes this DFA by computing equivalent states and merging them.
	 * @param replaceStateNames - if true the algorithm will replace the merged state names (which can get very large and confusing very fast)
	 * with integers starting from 1
	 * @return the minimum DFA still accepting the same language as this DFA
	 * @throws IllFormedAutomatonException when the minimization did not produce a start or accept state
	 * @throws DuplicateTransitionException when two transitions with the same input symbol originate from the same state
	 * @throws StateException when a specific state is not present in a set of states
	 */
	public DFA minimize(boolean replaceStateNames) throws IllFormedAutomatonException, DuplicateTransitionException, StateException
	{
		/* We can minimize a DFA by applying Hopcroft's algorithm. The algorithm works as follows:
		 * Step 1: Compute the set of equivalence classes of states by partioning the set of states.
		 * Step 2: Each block in the partition represents an equivalence class. 
		 *         Merge all states in each block to obtain the minimum amount of states.
		 * Step 3: Find the new start and accept states. They are the merged states (blocks in the partition) that
		 *         contain the old start and accept states.
		 * Step 4: Repair the transition function. Each state in each equivalence class behaves the same for all inputs
		 *         so we can just take a single state from each block and use that as the basis for the new transition
		 *         for a given input symbol.
		 */
		
		// Hopcroft's algorithm for computing equivalence classes
		
		// P = {F, Q \ F}
		Set<Set<State>> P = new HashSet<>();
		P.add(new HashSet<>(acceptStates));
		P.add(Utility.symmetricDifference(states, acceptStates));
		
		// W = {F}
		Set<Set<State>> W = new HashSet<>();
		W.add(new HashSet<>(acceptStates));
		
		// while (W is not empty) do
		while (!W.isEmpty())
		{
			// choose and remove a set A from W
			Set<State> A = W.iterator().next();
			W.remove(A);
			// for each c in sigma do	
			for (String c : alphabet)
			{
				// let X be the set of states for which a transition on c leads to a state in A
				Set<State> X = new HashSet<>();
				for (Transition transition : transitionFunction)
				{
					//if (transition.getSymbol().equals(c) && A.contains(transition.getNewState()))
					if (transition.getSymbol().equals(c) && Utility.contains(A, transition.getNewState()))
					{
						X.add(transition.getOriginState());
					}
				}
				// for each set Y in P for which X intersection Y is nonempty and Y \ X is nonempty do
				Set<Set<State>> newP = new HashSet<>();
				for (Set<State> Y : P)
				{
					Set<State> intersection = Utility.intersection(X, Y);
					Set<State> symmetricDifference = Utility.symmetricDifference(Y, X);
					if (!intersection.isEmpty() && !symmetricDifference.isEmpty())
					{
						// replace Y in P by the two sets X intersection Y and Y \ X
						newP.add(intersection);
						newP.add(symmetricDifference);
						// if Y is in W
						if (W.contains(Y))
						{
							// replace Y in W by the same two sets
							W.remove(Y);
							W.add(intersection);
							W.add(symmetricDifference);
						}
						// else
						else
						{
							// if |X intersection Y| <= |Y \ X|
							if (intersection.size() <= symmetricDifference.size())
							{
								// add X intersection Y to W
								W.add(intersection);
							}
							// else
							else
							{
								// add Y \ X to W
								W.add(symmetricDifference);
							}
						}
					}
					else
					{
						newP.add(Y);
					}
				}
				P = newP;
			}
		}
		
		// in case each state is in the same equivalence class, i.e. the automaton accepts every string we need to
		// remove the empty block in the partition generated by the algorithm
		Set<Set<State>> newP = new HashSet<>();
		for (Set<State> block : P)
		{
			if (!block.isEmpty())
			{
				newP.add(block);
			}
		}
		P = newP;
		
		// each block in the partition is an equivalence class, we merge the states in each equivalence class
		Set<State> newStates = new HashSet<>();
		Set<State> newAcceptStates = new HashSet<>();
		State newStartState = null;
		for (Set<State> block : P)
		{
			// merge the states in the current block
			State mergedState = new State(consolidateStateName(block));
			newStates.add(mergedState);
			
			// the new start state is the block of the partition containing the old start state
			for (State state : block)
			{
				if (state.equals(startState))
				{
					newStartState = mergedState;
					break;
				}
			}
			
			// the new accept states are the blocks of the partition containing the old accept states
			outerLoop: for (State state : block)
			{
				for (State accept : acceptStates)
				{
					if (state.equals(accept))
					{
						newAcceptStates.add(mergedState);
						break outerLoop;
					}
				}
			}
		}
		if (newStartState == null)
		{
			throw new IllFormedAutomatonException("The minimized states of the DFA do not contain the original start state!");
		}
		if (newAcceptStates.isEmpty())
		{
			throw new IllFormedAutomatonException("The minimized states of the DFA do not contain any of the original accept states!");
		}
		
		// now fix the transition function
		Set<Transition> newTransitionFunction = new HashSet<>();
		for (Set<State> block : P)
		{
			// pick a state from this block
			State s = block.iterator().next();
			// and for every symbol in the alphabet generate a new transition going from this block
			for (String symbol : alphabet)
			{
				// find the new state from the transition originating in the picked state with the current symbol
				State newState = null;
				for (Transition transition : transitionFunction)
				{
					if (transition.getSymbol().equals(symbol) && transition.getOriginState().equals(s))
					{
						newState = transition.getNewState();
					}
				}
				if (newState == null)
				{
					throw new IllFormedAutomatonException("There was no transition found for an input symbol for an old state!");
				}
				// now find the block in the partition containing that new state, this will be the new state for the new transition
				Set<State> blockContainingNewState = null;
				outerLoop: for (Set<State> states : P)
				{
					for (State state : states)
					{
						if (state.equals(newState))
						{
							blockContainingNewState = states;
							break outerLoop;
						}
					}
				}
				if (blockContainingNewState == null)
				{
					throw new IllFormedAutomatonException("One of the old transitions' new state was not found in the blocks of the partition!");
				}
				
				// and finally add the new transition with the origin state being this current block and the new state
				// being the block containing the state found by picking a state s from this block and searching for another
				// block containg the new state from the transition originating in state s with the current symbol
				newTransitionFunction.add(new Transition(
						findStateWithName(newStates, consolidateStateName(block)),
						symbol,
						findStateWithName(newStates, consolidateStateName(blockContainingNewState))));
			}
		}
		
		// replace the names of all states with integers starting from one (unreplaced state names can get quite large very fast
		// because when the algorithm merges a set of states it also merges their names)
		if (replaceStateNames)
		{
			int newStateName = 1;
			for (State state : newStates)
			{
				state.setName(newStateName++);
			}
		}
		
		return new DFA(newStates, alphabet, newTransitionFunction, newStartState, newAcceptStates);
	}
	
	/**
	 * Removes all unreachable states from the given set of states, the transition function and the set of accept states.
	 * A state is unreachable if there exists no path from the start state to that state. This method acts directly upon the given
	 * parameter objects. It does not return copies of the parameters nor does it preserve their original values.
	 * @param states - the set of states
	 * @param transitionFunction - the transition function describing the transitions between states
	 * @param startState - the state to start from
	 * @param acceptStates - the set of accept states, a subset of the set of states
	 */
	private void removeUnreachableStates(Set<State> states, Set<Transition> transitionFunction, State startState, Set<State> acceptStates)
	{
		Set<State> reachableStates = reachableStatesFromState(states, transitionFunction, startState, new HashSet<>());
		Set<State> unreachableStates = new HashSet<>();
		for (State state : states)
		{
			if (!Utility.contains(reachableStates, state))
			{
				unreachableStates.add(state);
				Set<Transition> unreachableTransitions = new HashSet<>();
				for (Transition transition : transitionFunction)
				{
					if (transition.getOriginState().equals(state) || transition.getNewState().equals(state))
					{
						unreachableTransitions.add(transition);
					}
				}
				transitionFunction.removeAll(unreachableTransitions);
			}
		}
		states.removeAll(unreachableStates);
		acceptStates.removeAll(unreachableStates);
	}
	
	/**
	 * Finds all reachable states in the set of states from the specified starting state.
	 * @param states - the set of states
	 * @param transitionFunction - the transition function
	 * @param startState - the state to start from
	 * @param visitedStates - the set of states already visited (and should be excluded from further recursion to avoid infinite loops)
	 * @return a set containing all reachable states (i.e. there is a continuous path from the start state to any reachable state)
	 */
	private Set<State> reachableStatesFromState(Set<State> states, Set<Transition> transitionFunction, State startState, Set<State> visitedStates)
	{
		Set<State> reachableStates = new HashSet<>();
		reachableStates.add(startState);
		visitedStates.add(startState);
		
		for (Transition transition : transitionFunction)
		{
			if (transition.getOriginState().equals(startState) && !Utility.contains(visitedStates, transition.getNewState()))
			{
				reachableStates.addAll(reachableStatesFromState(states, transitionFunction, transition.getNewState(), visitedStates));
			}
		}
		
		return reachableStates;
	}
	
	/**
	 * Finds a state with the specified name in the set of given states.
	 * @param states - the set of states to search for a state with the specified name in
	 * @param name - the name of the state to search for
	 * @return a state with the specified name, null if no such state was found
	 */
	private State findStateWithName(Set<State> states, String name) throws StateException
	{
		for (State state : states)
		{
			if (state.getName().equals(name))
			{
				return state;
			}
		}
		
		throw new StateException("Found no state with name: " + name + "!");
	}
	
	/**
	 * Consolidates the names of all states in the given set.
	 * @param states - the set of states to generate a name from
	 * @return a state name consisting of all the names from the states from the given set
	 */
	private String consolidateStateName(Set<State> states)
	{
		String stateName = "{";
		for (State state : states)
		{
			stateName += state.getName() + ",";
		}
		if (stateName.length() > 1)
		{
			stateName = stateName.substring(0, stateName.length()-1); // delete the last comma
		}
		else
		{
			stateName += "empty";
		}
		stateName += "}";
		
		return stateName;
	}
	
	/**
	 * Returns the number of transitions in this automaton.
	 */
	public int transitionCount()
	{
		return transitionFunction.size();
	}
	
	/**
	 * Calculates per number of incoming transitions the number of states that have this amount of incoming transitions.
	 * @return an array whose indices are the number of incoming transitions and values the number of states 
	 * with this amount of incoming transitions
	 */
	private int[] statesPerIncomingTransitionCount()
	{
		int[] stateCount = new int[transitionFunction.size() + 1]; // max size of the array will always be the total number of transitions
		int incomingTransitionCount = 0;
		// for every state
		for (State state : states)
		{
			// check every transition
			for (Transition transition : transitionFunction)
			{
				// and if that transition's new state matches the current iteration's state
				if (transition.getNewState().equals(state))
				{
					// up the incoming transition count
					incomingTransitionCount++;
				}
			}
			stateCount[incomingTransitionCount]++;
			incomingTransitionCount = 0;
		}
		
		return stateCount;
	}
	
	/**
	 * Prints statistics on the transitions of this DFA. Includes the total number of transitions and per number of incoming
	 * transitions the amount of states that have this number of incoming transitions.
	 */
	public void printTransitionReport()
	{
		int[] stateCount = statesPerIncomingTransitionCount();
		
		//Log.info("Total amount of transitions: " + transitionFunction.size());
		for (int i=0; i<stateCount.length; i++)
		{
			if (stateCount[i] != 0)
			{
				Log.info("\tThere are " + stateCount[i] + " states with " + i + " incoming transitions.");
			}
		}
	}
	
	/**
	 * Returns a string containing the formal description of this DFA.
	 */
	public String toString()
	{
		String result = "[DFA]\n";
		
		result += "--States:--\n";
		for (State state : states)
		{
			result += state.getName() + "\n";
		}
		result += "--Alphabet:--\n";
		for (String symbol : alphabet)
		{
			result += symbol + "\n";
		}
		result += "--Transition function:--\n";
		for (Transition transition : transitionFunction)
		{
			result += transition.toString() + "\n";
		}
		result += "--Start state:--\n" + startState.getName() + "\n";
		result += "--Accept states:--\n";
		for (State state : acceptStates)
		{
			result += state.getName() + "\n";
		}
		result = result.substring(0, result.length()-1);
		
		return result;
	}
	
	public Set<State> getStates()
	{
		return states;
	}
	
	public Set<String> getAlphabet()
	{
		return alphabet;
	}
	
	public Set<Transition> getTransitionFunction()
	{
		return transitionFunction;
	}
	
	public State getStartState()
	{
		return startState;
	}
	
	public Set<State> getAcceptStates()
	{
		return acceptStates;
	}
}
