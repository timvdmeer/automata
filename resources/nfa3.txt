[States]
1
2
3

[Alphabet]
a
b

[Transition Function]
(1,epsilon)=3
(1,b)=2
(2,a)=2
(2,a)=3
(2,b)=3
(3,a)=1

[Start State]
1

[Accept States]
1